package com.unomic.dulink.chart.domain;


import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;



@Getter
@Setter
@ToString
public class TimeChartVo{
	
//	@Getter
//	@Setter
//	@ToString
//	public class Chart{
//		List<Data> data;
//		String color;
//	}
//	
//	@Getter
//	@Setter
//	public class Data{
//		String y;
//		String startTime;
//		String endTime;
//	}
//	

		String color;
		String y;
		String startTime;
		String endTime;
	
	
	
}



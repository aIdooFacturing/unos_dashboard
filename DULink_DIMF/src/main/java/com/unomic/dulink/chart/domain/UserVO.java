package com.unomic.dulink.chart.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UserVO {
	
	String id;
	String pwd;
	String shopId;
	String name;

}

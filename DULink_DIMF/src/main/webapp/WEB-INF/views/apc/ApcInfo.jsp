<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->
<!-- <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script> -->
<%-- <script src="${ctxPath}/js/dataTables/js/jquery-1.11.2.min.js"></script> --%>
<script src="${ctxPath}/js/dataTables/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="${ctxPath}/js/dataTables/css/jquery.dataTables.css">
<script type="text/javascript">
	var csvOutput;
	$(function(){
		today();
		$("#tabs").tabs();
	});
	
	function today(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth() +1));
		var day = addZero(String(date.getDate()));
		
		$("#visitorForm input[id='sdate']").val(year + "-" + month);
		$("#visitorForm input[id='edate']").val(year + "-" + month);
		$(".sdate").val(year + "-" + month + "-" + day);
		$(".edate").val(year + "-" + month + "-" + day);
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	
	function replacePlus(str){
		str = str.replace(/\+/gi," ");
		return str;
	};
	
	function replace(str){
		str = str.replace(/-/gi,".");
		return str;
	};
	

	
	function getApcList(){
		var url = "${ctxPath}/adapter/getApcList.do";
		
		var sdate = $('#sdate').val();
		var edate = $('#edate').val();
		var param = "sDate=" + sdate + "&eDate=" + edate;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "html",
			type : "post",
			success : function(data){
				$("#apcList").html('');
				$("#apcList").html(data);
				if ( $.fn.dataTable.isDataTable( '#apcList' ) ) {
					$("#apcList").dataTable({
						"destroy": true
					});
					$("#apcList").dataTable();
				}else{
					dataTable = $("#apcList").dataTable();
				};		
			}
		});
	};
	
	
	
	function csvData(ty, title){
		console.log('run csvData');
		var url = "";
		var param = "";
		if(ty=="apc"){
			url = "${ctxPath}/adapter/getApcListCSV.do";
			var sdate = $('#sdate').val();
			var edate = $('#edate').val();
			param = "sDate=" + sdate + 
						"&eDate=" + edate;
		}
		
		$.ajax({
			url : url,
			type : "post",
			data : param,
			dataType : "json",
			success : function(json) {
				if(ty=="apc"){
					csvOutput = "NO, 설비명, 설비상태 , 프로그램명 , 시작시간, 종료시간, 프로그램 종류, 작업기준일  LINE";
					var dscnt = json;
					var i = 1;
					$(dscnt).each(function(){
						var dvcName = replacePlus(decodeURIComponent(this.dvcName));
						var chartStatus = replacePlus(decodeURIComponent(this.chartStatus));
						var prgmName = replacePlus(decodeURIComponent(this.prgmName));
						var startDateTime = replacePlus(decodeURIComponent(this.startDateTime));
						var endDateTime = replacePlus(decodeURIComponent(this.endDateTime));
						var prgmType = replacePlus(decodeURIComponent(this.prgmType));
						var date = replacePlus(decodeURIComponent(this.date));
						
						
						csvOutput += i + "," + 
										dvcName + "," +
										chartStatus + "," +
										prgmName + "," +
										startDateTime + "," +
										endDateTime + "," +
										prgmType + "," +
										date + " LINE";
						i++;
					});
				}
				csvSend(title);
			}
		});
	};
	
	function csvSend(title){
		var sdate = title;
		var edate = "";
		if(title=="온도 센서 정보"){
			var date1 = $('#sdate').val();
			var date2 = $('#edate').val();
			edate = date1 + " - " + date2;			
		}

		csvOutput = csvOutput.replace(/\n/gi,"");
		f.csv.value=encodeURIComponent(csvOutput);
		f.startDate.value = sdate;
		f.endDate.value = edate;
		f.submit(); 
	};
	
	function setInit(ty){
		if(ty=="apc"){
			getApcList();	
		};
	};
</script>
<style type="text/css">
</style>
<title>APC Information</title>
</head>
<body>
	
	<div id="tabs" style="width: 100%">
		<ul>
		    <li><a href="#tab1" onclick="setInit('apc')">APC 정보</a></li>
	  	</ul>
	  	
	   <div id="tab1">
			<center>
				<div style="font-weight: bold;">※ 날짜를 선택 하세요. ※</div>
				<form action="" id="apcListForm">
					<input type="date" id="sdate" class="sdate" data-role="none" > - 
					<input type="date" id="edate" class="edate" data-role="none" >
					<button onclick="getApcList(); return false;" data-role="none"  class="csvBtn">조회</button>
					<button onclick="csvData('apc','설비 apc 정보'); return false;" data-role="none" id="csvBtn" class="csvBtn">엑셀 저장</button>
					<table border="1" width="100%" id="apcList" style="text-align: center;border-collapse: collapse; font-size: 9px">
					</table>
				</form>
			</center>
	   </div>
	</div>
	<!--csv  -->
	<form action='${ctxPath}/adapter/csv.do' id="f" method="post">
			<input type="hidden" name="csv" value="">
			<input type="hidden" name="startDate" value="">
			<input type="hidden" name="endDate" value="">
	</form>
</body> 
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1">
<title>CNC Information</title>
<script src="${ctxPath }/js/jq.js" ></script>
<script src="${ctxPath }/js/RGraph/RGraph.common.core.js" ></script>
<script src="${ctxPath }/js/RGraph/RGraph.common.dynamic.js" ></script>
<script src="${ctxPath }/js/RGraph/RGraph.common.effects.js" ></script>
<script src="${ctxPath }/js/RGraph/RGraph.gauge.js" ></script>
<script src="${ctxPath }/js/RGraph/RGraph.gantt.js" ></script>
<script src="${ctxPath }/js/RGraph/RGraph.hbar.js" ></script>
<script src="${ctxPath }/js/RGraph/RGraph.hprogress.js" ></script>
<script src="${ctxPath }/js/jit/jit.js" ></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
	var width = window.innerWidth;
	var height = window.innerHeight;
	google.load("visualization", "1", {packages:["corechart"]});
	var myInterval;
	var intervalValue = 1000;
	var ip;
	var port;
	var status;
	var pStatus;
	var sHour;
	var maxRPM = 0;
	var timeline_data;
	var time_line;
	var spindle_load_chart;
	var feed_override_chart;
	var spindle;
	var feed;
	var status_chart;
	var sTime;
	
	var status_chart_data;
	var status_chart_options;
	<%
		String ip = (String) session.getAttribute("ip");
		String port = (String) session.getAttribute("port");
	%>
	ip = "<%=ip%>";
	port = "<%=port%>";
	
	$(function(){
		/* $("#status_chart").css("width", width * 0.9);
		$("#spindle_chart").css("width",width * 0.9);
		$("#feed_override_chart").css("width", width * 0.9); */
		//google chart
		status_chart_options = {
		        width: 350,
		        height: 45,
		        legend: { position: 'none', maxLines: 3 },
		        bar: { groupWidth: '75%' },
		        isStacked: true,
		        hAxis : {
		        	maxValue : 24
		          },
		        chartArea : {
		        	width : '350',
		        	height : '25',
		        	top : "5",
		        	left : "5"
		          },
		        bar:{
		        	groupWidth : '95%'
		         },
		      	 backgroundColor : "#E5E4E2",
		      	 tooltip : {
		      		 trigger: 'none'
		      	 }
		      };
		status_chart = new google.visualization.BarChart(document.getElementById('status_chart'));
		
		$("#interval").val(intervalValue/1000);
		
	   spindle_load_chart = new RGraph.HProgress({
            id: 'spindle_chart',
            min: 0,
            max: 160,
            value:0,
            options: {
                tickmarks: 100,
                numticks: 20,
                gutter: {
                	right: 13,
                   left : 10,
                   top:10,
                  	bottom : 20
               	 	},
                margin: 5,
                units: {
                   post: '%'
                	},
                text:{
   	        	  		size:7
   	          		},
            	}
        }).draw();
	  	
	   feed_override_chart = new RGraph.HProgress({
           id: 'feed_override_chart',
           min: 0,
           max: 200,
           value:0,
           options: {
               tickmarks: 100,
               numticks: 20,
               gutter: {
	               	right: 13,
                   	left : 10,
                   	top : 10,
                   	bottom :20
               	  },
               units: {
                   post: '%'
              	  },
               margin: 5,
            	  text:{
 	        	  		size:7
 	          	  },
           		}
       }).draw();
	   
	  	getDataLoop();
	});
	
	function getDataLoop(){
		myInterval = setInterval(getData,intervalValue);
	};
	
	var flag = true;
	var pre_msgArray = new Array();
	var progName;
	var feedOverride;
	var progName;
	var spindleValues;
	var spindleLoad;
	var progress;
	function getData(){
		var date = new Date();
		var hour = date.getHours();
		var minute = date.getMinutes();
		var time = hour + (1/60).toFixed(2) * minute;
		var second = date.getSeconds();
		var url = "${ctxPath}/nfc/conn.do";
		var param = "ip=" + ip + 
						"&port=" + port;

		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				sTime = hour;
				$("#currentTime").html(addZero(String(hour)) + " : " + addZero(String(minute)) + " : " + addZero(String(second)));
				
				var index = data.split("/");
				spindle = index[0];
				feedOverride = index[1];
				$("#main_prog_name").html("<center><h2><b>" + index[2] +"</b><h2></center>");
				alarmNum = index[3];
				status = index[4];
				
				//feedOverride = 100;				
				spindleValues = index[8];
				
				var spindleIdx = spindleValues.indexOf("%");
				spindleLoad = spindleValues.substring(0,spindleIdx); 
				var alarmMsg = removePlus(decodeURIComponent(index[5]));
				if(status=="STRT" ||status=="START"){
					stateColor = "green";
				}else{
					stateColor = "yellow";
				};
				
				var arrayIndex = alarmMsg.split("\n");
				var alarmMsgArray = new Array();
				alarmMsgArray.push(arrayIndex[0]);
				alarmMsgArray.push(arrayIndex[1]);
				alarmMsgArray.push(arrayIndex[2]);
				
				if(alarmMsg!=""){
					for(var i = 0; i < alarmMsgArray.length-1; i++){
						var idx = alarmMsgArray[i].indexOf(":");
						var lidx = alarmMsgArray[i].lastIndexOf(":");
						var alarmTy = alarmMsgArray[i].substring(lidx+1) + "_";
						if(alarmTy=="0_"){
							alarmTy = "";
						};
						$("#alarmCd" + (i+1)).html(alarmTy + alarmMsgArray[i].substring(0,idx));
						$("#alarmMsg" + (i + 1) + "-1").html(alarmMsgArray[i].substring(idx+2,lidx));
					};
					
					time_loop("red", time, addZero(String(minute)));
				}else{
					$(".alarmCd").html("　");
					$(".alarmMsg").html("　");
					time_loop(stateColor, time, addZero(String(minute)));
				};

				
				spindle_loop();
				feed_loop();
				
			},
			error : function(){
				time_loop("black",time, addZero(String(minute)));
			}
		});
	};
	
	function spindle_loop(){
		//var spindleLoad=0;
		//while(spindleLoad<75 && spindleLoad>85){
		//	spindleLoad = parseInt(Math.random()*100);
		//};
		/* var rnd = parseInt(Math.random()*100);
		rnd = 75+ (rnd % 10);
		spindleLoad = rnd;
*/
		var color = "";
		if(spindleLoad>=120){
			color = "red";
		}else if(spindleLoad>=80){
			color = "yellow";	
		}else{
			color = "green";
		}; 
		
		spindle_load_chart.value=Number(spindleLoad);
		spindle_load_chart.set('colors', [color]);
		spindle_load_chart.grow();
		
		$("#spindle_value").html(spindleLoad + "%");
	};
	
	function feed_loop(){
		var color = "";
		if(feedOverride>=150){
			color = "red";
		}else if(feedOverride>=100){
			color = "yellow";	
		}else{
			color = "green";
		};
		feed_override_chart.value=Number(feedOverride);
		feed_override_chart.set('colors', [color]);
		feed_override_chart.grow();

		$("#feed_override_value").html(feedOverride + "%");
	};
	
	function removePlus(str){
		return str = str.replace(/\+/gi," ");
	};
	
	function setChartInterval(){
		var interval = $("#interval").val()*1000;
		intervalValue = interval;
		
		clearInterval(myInterval);
		getDataLoop();	
	};
	
	function resetIntervalVal(){
		$("#interval").val("");
	};
	
	function sendURI(){
		var url = window.location.protocol + "//" + window.location.host + window.location.pathname;
		location.href="ditalk://talkurl?confirm=" + url;
	};
	
	var pHour;
	var pStatus;
	var first = true;
	var cStatus;
	var statusData = [""];
	var style = {"role" : "style"};
	var statusTy = [""];
	var pMinute;
	var arrayIdx = 2;
	function time_loop(stateColor, time, minute){
		pStatus = stateColor;
		minute = String(minute).substring(0, 1);
		$("#currentStatus").css("background-color",stateColor);
		var status_ty;
		if(stateColor=="green"){
			status_ty = "In-cycle";
		}else if(stateColor=="yellow"){
			status_ty = "Wait";
		}else if(stateColor=="red"){
			status_ty = "Alarm";
		}else{
			status_ty = "black";
		};
		
		if(first){
			statusTy.push(status_ty);
			statusTy.push(style);
			statusData.push(time);
			statusData.push("#E5E4E2");
			
			status_chart_data = google.visualization.arrayToDataTable([
			     	                                                  statusTy,
			     	                                                  statusData
			     	                                                ]);
			
			status_chart.draw(status_chart_data, status_chart_options);
			first = false;
		};
		
		if(pMinute!=minute){
			
			statusTy.push(status_ty);
			statusTy.push(style);
			statusData.push(0.165);
			statusData.push(stateColor);
			
			status_chart_data = google.visualization.arrayToDataTable([
			     	                                                  statusTy,
			     	                                                  statusData
			     	                                                ]);
			
			status_chart.draw(status_chart_data, status_chart_options);
			pMinute = minute;
			arrayIdx+=2;
		};
		
		if(stateColor=="red" || (pStatus=="green" && stateColor == "yellow") && stateColor!="black"){
			statusData[arrayIdx]=stateColor;
			status_chart_data = google.visualization.arrayToDataTable([
				     	                                                  statusTy,
				     	                                                  statusData
				     	                                                ]);
				
			status_chart.draw(status_chart_data, status_chart_options);
			pMinute = minute;
		};
		
		var date = new Date();
		var hour = date.getHours();
		var minute = date.getMinutes();
		var second = date.getSeconds();
		$("#now").html(addZero(String(hour)) + " : " + addZero(String(minute)) + " : " + addZero(String(second)));
	};
	
	function addZero(str){
		if(str.length=="1"){
			str = "0" + str;
		};
		return str;
	};
</script>
<style type="text/css">
	.circle{
		height: 10px;
		padding: 10px 5px 10px 5px;
		display: none;
		border-radius: 100%;
	   -o-border-radius: 100%;
	   -webkit-border-radius: 100%;
	   -moz-border-radius: 100%;
	}
	.label{
		font-size: 13px;
		margin-left: 3px;
	}
</style>
</head>
<body>
	<Table width="100%" style="margin: 0px; ">
		<tr >
			<td colspan="2">갱신주기 : 
				<input id="interval" type="tel" size="2" onfocus="resetIntervalVal();">(초)
				<button onclick="setChartInterval();" >설정</button>
				<button onclick="sendURI()">공유</button>
				<span id="currentTime" style="margin-left: 10px"></span>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<span id="main_prog_name"></span>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<span style="background-color: green; padding: 0.2px;">　</span><font class="label" >In-cycle</font>
				<span style="background-color: yellow; padding: 0.2px;">　</span><font class="label">Wait</font>
				<span style="background-color: red; padding: 0.2px;">　</span><font class="label">Alarm</font>
				<span style="background-color: black; padding: 0.2px;">　</span><font class="label">No Connection</font>　
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<div id="status_chart" width="350" height="100"></div>
				<!-- <div id="status_chart"></div> -->
				<div>Current Status : <span style="padding: 0.2px;" id="currentStatus">　</span></div>
			</td>
		</tr>
		<tr>
			<Td colspan="2">
				<table width="100%" border="1" style="border-collapse: collapse;">
					<tr>
						<td colspan="3">
							<b>Alarm</b>
						</td>
					</tr>
					<tr>
						<Td  width="20%" id="alarmCd1" align="right" class="alarmCd">　</Td>
						<td id="alarmMsg1-1" class="alarmMsg">　</td>							
					</tr>
					<tr>
						<Td  width="20%" id="alarmCd2" align="right" class="alarmCd">　</Td>
						<td id="alarmMsg2-1" class="alarmMsg">　</td>							
					</tr>
				</table>
			</Td>
		</tr>
		<tr>
			<td colspan="2" >
				Spindle Load : <span id="spindle_value"></span>
				<canvas id="spindle_chart" width="350" height="80"></canvas>
				<%-- <canvas id="spindle_chart"  height="80"></canvas> --%>
			</td>
		</tr>
		<tr>
			<td colspan="2" >
				Feed Override : <span id="feed_override_value"></span>
				<canvas id="feed_override_chart" width="350" height="80"></canvas> 
				<%-- <canvas id="feed_override_chart"  height="80"></canvas> --%>
			</td>
		</tr>
	</Table>
</body>
</html>
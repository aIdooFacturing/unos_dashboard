<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<spring:message code="start" var="start"></spring:message>
<spring:message code="complete" var="complete"></spring:message>
<spring:message code="worker" var="worker"></spring:message>
<spring:message code="content" var="content"></spring:message>
<spring:message code="opratio_against_daily_target" var="opratio_against_daily_target"></spring:message>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<style type="text/css">
	.selected_span{
	background-color :white;
	color : #008900;
}
	
</style>
<title>Dash Board</title>
<script type="text/javascript">
	var fromDashboard = "${fromDashBoard}";
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
	
	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var start = "${start}"
	var complete = "${complete}"
	var worker = "${worker}"
	var content = "${content}"
	var opratio_against_daily_target= "${opratio_against_daily_target}";
</script>
<style type="text/css">
	.selected_menu{
	background-color :rgb(33,128,250);
	color : white;
}

.unSelected_menu{
	background-color :gray;
	color : white
}
	
</style>
<script type="text/javascript" src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery-ui.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery.circle-diagram.js"></script>
<script src="${ctxPath }/js/jqMap.js"></script>
<script src="${ctxPath }/js/chart/highstock_data.js"></script>	
<script type="text/javascript" src="${ctxPath }/js/highstock.js"></script>
<script type="text/javascript" src="${ctxPath }/js/highcharts-3d.js"></script>
<script src="${ctxPath }/js/multicolor_series.js"></script>
<script src="${ctxPath }/js/detailChart_rotate.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/card.css">
</head>

<body >
	<div id="bannerDiv">
		<Center>
			<input type="text" id="banner" size="100"><input type="color" id="color"><br>
			<button onclick="addBanner();">적용</button>
			<button onclick="cancelBanner();">미적용</button>
		</Center>
	</div>
	<div id="intro_back"></div>
	<span id="intro"></span>
		
	
		<img src="${ctxPath }/images/home.png" id="menu_btn" >
	<div id="corver"></div>
	
	<form action='${ctxPath}/csv/csv.do' id="f"  method="post">
		<input type="hidden" name="csv" value="">
		<input type="hidden" name="startDate" value="">
		<input type="hidden" name="endDate" value="">
	</form>
	
	<img alt="" src="${ctxPath }/images/close_btn.png" id="close_btn">
		<table id="time_table">
			<tr>
				<td id="title_right">
				</td>
			</tr>
			<tr>
				<Td>
					<font id="date"></font>
					<font id="time"></font>				
				</Td>
			</tr>
		</table>
		
	<div id="alarmBox">
		<table id="alarmTable" style="width: 100%">
		</table>
	</div>
	
	<div id="repairBox">
		<table id="repairTable" style="width: 100%">
			
		</table>
	</div>
	
	<div id="corver"></div>
	
	<!-- Part1 -->
	<div id="part1" class="page" style="background-color:  rgb(16, 18, 20)">
		<span id="detailRepBox"></span>
		<div id="mainTable2">
			<center>
				<table class="mainTable" >
					<tr>
						<Td align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="title" colspan="8" id="title_main">
							<spring:message code="dailydevicestatus"></spring:message>
						</Td>
					</tr>
					<Tr class="tr1" >
						<td colspan="5" width="60%" rowspan="2" valign="top">
						<div align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="subTitle">
								<spring:message code="status"></spring:message>
							</div>
							<div id="container"></div>
						</td>
						<td width="40%" valign="top" id="machine_name_td" colspan="3">
							<div align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;vertical-align: middle;" class="subTitle">
								<span id="toolLabel"><spring:message code="device"></spring:message> </span>	<img alt="" src="${ctxPath }/images/tool.png" id="tool" style="vertical-align: middle;">
							</div>
							<select id="dvcName1"></select><br>
							<span id="dvcTy"></span>
						</td>		
					</Tr >
					<tr class="tr1">
						<td valign="top" id="alarm_td" colspan="3">
							<div align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="subTitle" id="test_div">
								<spring:message code="alarm"></spring:message>	<img alt="" src="${ctxPath }/images/alarm_mark.png" id="alarm_mark" style="vertical-align: middle;">
							</div>
							<div id="alarm" align="left">
							</div>
						</td>
					</tr>
					<tr class="tr2" valign="top" id="chartTr">
						<td width="12.5%">
							<div align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="subTitle">
							<spring:message code="dailyprdcttarget"></spring:message>
							</div>	
							<div class="neon1" id="daily_target_cycle">
							 	-
							 </div>
						</td>
						<td width="12.5%">
							<div align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="subTitle">
								<spring:message code="dailyprdctcnt"></spring:message>		
							</div>
							<div class="neon2">
								<span id="complete_cycle">
									-
								</span>
								<font id="downValue" class="remains_cycle">
									-
								</font>
							</div>
						</td>
						<td width="12.5%">
							<div align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="subTitle">
								<spring:message code="prdct_per_cycle"></spring:message>		
							</div>
							<div class="neon2" id="prdctPerCyl">
							</div>
						</td>
						<td width="12.5%">
							<div align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="subTitle">
								<spring:message code="daily_cycle_cnt"></spring:message>
							</div>
							<div class="neon2" id="cylCnt">

							</div>
						</td>
						<td width="12.5%">
							<div align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="subTitle">
							<spring:message code="daily_prdct_avrg_cycle_time"></spring:message>
							</div>
							<div class="neon1" id="daily_avg_cycle_time">
								-
							</div>
						</td>
						<td width="12.5%">
							<div align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="subTitle">
							<spring:message code="daily_prdct_cnt_per_hour"></spring:message>
							</div>
							<div class="neon3"  id="daily_length">
								-
							</div>
						</td>
						<td width="12.5%">
							<div align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="subTitle" id="tool_title">
								Feed Override
							</div>
							<div class='neon3' id="feedOverride">-</div>
						</td>
						<td width="12.5%">
							<div align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="subTitle" id="tool_title">
								Spindle Load
							</div>
							<div class='neon3' id="spdLoad">-</div>
						</td>
					</tr>
					<tr>
						<Td align="center" style="color:white; font-weight: bolder;" class="title" colspan="8" id="lastCell">
							<div id="barChart"></div>
						</Td>
					</tr>
				</table>
			</center> 
		</div>
	</div>
</body>
</html>
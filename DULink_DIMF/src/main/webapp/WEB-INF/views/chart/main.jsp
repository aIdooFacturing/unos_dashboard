<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />

<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">


<title>Dash Board</title>
<style type="text/css">
body{
	background-color : black;
}

</style>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
		
</script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>

<link rel="stylesheet" href="${ctxPath }/css/default.css">
<script type="text/javascript">
	var login_lv = window.sessionStorage.getItem("lv");

	
	$(function(){
		setEl();
		/* var login = window.sessionStorage.getItem("login");
		var login_time = window.sessionStorage.getItem("login_time");
		var time = new Date().getTime();
		if(login!=null && (time - login_time) / 1000 < 60*20) */ 
		timer();
		bindEvt();
	});
	
	function bindEvt(){
		$("img").not("#grid, #aIdoo, .flag, #logo, #logo2, #enter, #logout").hover(function(){
			var id = $(this).attr("name");
			$(this).attr("src", "${ctxPath}/images/icons/" + id + "_select.png");
		}, function(){
			var id = $(this).attr("name");
			$(this).attr("src", "${ctxPath}/images/icons/" + id + ".png");
		})
		
	};
	
	var handle = 0;
	function timer(){
		$("#today").html(getToday());
		handle = requestAnimationFrame(timer);
	};
	
	function setEl(){
		$("#link").css({
			"color" : "white",
			"text-decoration" : "underline",
			"font-size" :getElSize(50),
			"font-weight" : "bolder",
			//"cursor" : "pointer",
			"position" : "absolute",
			"right" : getElSize(50),
			"bottom" : getElSize(150)
		}).click(function(){
			//location.href = "http://mtmes.doosanmachinetools.com/DULink/chart/main.do";
		});
		
		
		$("#sign-in").hover(function() {
			$("#sign-in").css({
				"background" : "black",
				"color" : "white"
			});
		});
		
		
		$("body").css({
			"margin" : 0,
			"padding" : 0,
			/* "background" : "url(" + ctxPath + "/images/home_bg.png)",
			"background-size" : "100%", */
			"background-color" : "black",
			"overflow" : "hidden"
		});
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
			"background-color" : "black ",
			"text-align" : "center"
		});
		
		$("#container").css({
			"margin-top" : (originHeight/2) - ($("#container").height()/2), 
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
		});
		
		$("#today").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#logo3").css({
			"margin-left" : getElSize(30)	
		});
		
		$("#iconTable img").css({
			"cursor" : "pointer",
			"height" : getElSize(450),
			//"margin-top" :getElSize(200)
		}).click(showMenu);
		
		$("#panel").css({
			"background-color" : "rgb(34,34,34)",
			"border" : getElSize(20) + "px solid rgb(50,50,50)",
			"position" : "absolute",
			"width" : contentWidth * 0.2,
			"top" : 0,
			"left" : -contentWidth * 0.2 - getElSize(40),
			"z-index" : 99999
		});
		
		
		$("#intro").css({
			/* "width" : contentWidth, */
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.3,
			"position" : "absolute",
			"background-color" : "white",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		});
		
		$("#iconTable").css({
			"margin-top" : getElSize(250) + marginHeight	
		});
		
		$("#iconTable td span").css({
			"color" : "white",
			"font-size" : getElSize(90)
		});
		
		$("#iconTable td").css({
			"text-align" : "center",
			"padding-left" : getElSize(200),
			"padding-right" : getElSize(200),
			"padding-top" : getElSize(100),
			"padding-bottom" : getElSize(100),
		});
		
		
		$("#iconTable img, span").css({
			"-webkit-filter" : "blur(0px)"
		});
		
		
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(80) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#grid").css({
			"position" : "absolute",
			"width" : contentWidth * 0.8,
		});
		
		$("#grid").css({
			"left" : (window.innerWidth/2) - ($("#grid").width()/2),
			//"top" : getElSize(100) + marginHeight
			"top" : getElSize(100) + marginHeight + getElSize(250)
		});
		
		$("#iconTable tr:nth(0) td img").not("#grid").css({
			"margin-top" : getElSize(100)
		});
		//chkBanner();
		
		$("#aIdoo").css({
			"position" : "absolute",
			"width" : getElSize(300)
		});
		
		$("#aIdoo").css({
			"left" : (window.innerWidth/2) - ($("#aIdoo").width()/2),
			"bottom" : marginHeight + getElSize(50)
		});
		
		$("#sign-in").css({
			"background": "white",
			"border" : "1px solid black",
			"font-size" : getElSize(50),
			"cursor": "pointer"
		})
		
		$("#sign-in").on("mouseleave", function() {
			$("#sign-in").css({
				"background" : "white",
				"color" : "black"
			})
			
		});
		
	};
	
	function goPage(el){
		var url = $(el).attr("url");
		if(url=="url"){
			alert("준비 중입니다.");
			return;
		}
		
		if($(el).attr("disable")=="true"){
			alert("권한이 없습니다.");
			return;
		};
		
		if(url == "banner"){
			getBanner();
			closePanel();
			$("#bannerDiv").css({
				"z-index" : 9999,
				"opacity" : 1
			});
			panel_flag = false;
			return;
		};
		
		location.href = "${ctxPath}/chart/" + url; 
	};
	 
	function showMenu(){
		var id = this.id;
		
		if(id=="monitor"){
			location.href = "${ctxPath}/chart/main.do?lang=" + localStorage.getItem("lang");
		}else if(id=="analysis"){
			location.href = "/Performance_Report_Chart/index.do?lang=" + localStorage.getItem("lang");
		}else if(id=="inven"){
			location.href = "${ctxPath}/chart/incomeStock.do?lang=" + localStorage.getItem("lang");
		}else if(id=="maintenance"){
			location.href = "/Alarm_Manager/index.do?lang=" + localStorage.getItem("lang");
		}else if(id=="order"){
			location.href = "${ctxPath}/chart/addTarget.do?lang=" + localStorage.getItem("lang");
		}else if(id=="quality"){
			location.href = "${ctxPath}/chart/checkPrdct.do?lang=" + localStorage.getItem("lang");
		}else if(id=="tool"){
			location.href = "${ctxPath}/chart/toolManager.do?lang=" + localStorage.getItem("lang");
		}else if(id=="kpi"){
			location.href = "${ctxPath}/chart/performanceAgainstGoal_chart_kpi.do?lang=" + localStorage.getItem("lang");
		}else if(id=="program_manager"){
			location.href = "/Program_Manager/index.do?lang=" + localStorage.getItem("lang");
		}else if(id=="configuration"){
			location.href = "/Device_Status/index.do?lang=" + localStorage.getItem("lang");
		}else if(id=="adv_report"){
			window.open(
					"http://52.185.156.250/Reports/browse/",
				  '_blank' // <- This is what makes it open in a new window.
				);
		}else if(id=="ml"){
			window.open(
					"https://studio.azureml.net/",
				  '_blank' // <- This is what makes it open in a new window.
				);
		}else if("remote"){
			 var ws = new WebSocket("ws://localhost:3000", "echo-protocol");
	
			 // 연결이 수립되면 서버에 메시지를 전송한다
			  ws.onopen = function(event) {
			    ws.send("Client message: Hi!");
			  }
	
			  // 서버로 부터 메시지를 수신한다
			  ws.onmessage = function(event) {
			  }
	
			  // error event handler
			  ws.onerror = function(event) {
			  }
			  
			  
		}
		
		
		
		
	};
	
	
	function showPanel(){
		$("#panel").animate({
			"left" : 0
		});
	};

	function closePanel(){
		$("#panel").animate({
			"left" : -contentWidth * 0.2 - getElSize(40)
		});
	};
	
	var dialog;
	$(function(){
		dialog = $("#dialog").kendoDialog({
			height: getElSize(1800),
			width : getElSize(1500),
			visible: false,
			animation: {
			    open: {
			      effects: "fade:in",
			      duration: 500
			    },
			    close: {
			        effects: "fade:out",
			        duration: 500
		        }
		  	},
			modal: true,
			content: "<h1 id='sign-title'>회원가입페이지</h1>" +
			"<hr>" +
			"<div id='id-div'>" +
			"<input id='id-input' type='text' placeholder='아이디' onkeyup='checkId(this)' maxlength='12'>" +
			"<p id='id-alert'></p>" + 
			"</div>" +
			"<hr>" + 
			"<div id='pwd-div'><input id='pw-input' type='password' autocomplete='new-password' placeholder='비밀번호' onkeyup='checkPw(this)' maxlength='12'></div>" +
			"<br>" +
			"<div id='pwd-div-repeat'>" +
			"<input id='pw-input-repeat' type='password' autocomplete='new-password' placeholder='비밀번호 재 확인' onkeyup='checkPw(this)' maxlength='12'>" +
			"<p id='pw-alert'></p>" + 
			"</div>" +
			"<hr>" +
			"<button id='actionBtn' onclick='doSignIn()'>가입하기</button>" + 
			"<button id='closeBtn' onclick='dialog.close()'>닫기</button>",
			closable: false,
		    title: false,
		    open: function() {
		    	
		    	$("#sign-title").css({
		    		"margin-top" : 0,
		    		"border-bottom" : getElSize(10) + "solid black"
		    	})
		    	
		    	$("#id-div, #pwd-div, #pwd-div-repeat").css({
		    		"height" : getElSize(300),
		    	})
		    	
		    	$("#actionBtn").css({
					"position" : "absolute",
					"bottom" : getElSize(100),
					"font-size" : getElSize(70),
					"font-weight" : "bolder"
				})
				
		    	$("#closeBtn").css({
					"position" : "absolute",
					"bottom" : getElSize(100),
					"font-size" : getElSize(70),
					"font-weight" : "bolder",
					"right" : getElSize(100)
				})
				
				$("label").css({
					"font-size" : getElSize(70)
				})
				
				$("#id-input, #pw-input, #pw-input-repeat").css({
					"font-size" : getElSize(70),
					"border" : "0px",
					"height" : getElSize(150)
				})
				
				$("#id-alert, #pw-alert").css({
					"color" : "red"
				})
				
				$("#actionBtn, #closeBtn").css({
					"background": "white",
					"border" : "1px solid black",
					"font-size" : getElSize(50),
					"padding" : getElSize(50)
				})
				
				$("#actionBtn").hover(function(){
					$("#actionBtn").css({
						"background": "black",
						"color" : "white"
					})
				})
				
				$("#actionBtn").on("mouseleave", function() {
					$("#actionBtn").css({
						"background": "white",
						"color" : "black"
					})
				})
				
				$("#closeBtn").hover(function(){
					$("#closeBtn").css({
						"background": "black",
						"color" : "white"
					})
				})
				
				$("#closeBtn").on("mouseleave", function() {
					$("#closeBtn").css({
						"background": "white",
						"color" : "black"
					})
				})
				
		    }
		}).data("kendoDialog");
	})
	
	function signIn(){
		dialog.open();
		
		$("#id-input, #pw-input, #pw-input-repeat").val("")
		$("#id-alert").html("")
		$("#pw-alert").html("")
	}
	
	function checkId(e){
		if($("#id-input").val().length>=5){
			var idRegex = /^[a-z0-9]+$/;
			var validId = $("#id-input").val().match(idRegex);
			
		    if(validId == null){
		    	$("#id-alert").css({
					"color" : "red"
				})
				passId=false;
		    	$("#id-alert").html("아이디에 공백 또는 특수 문자는 포함 될수 없습니다.")
		        return false;
		    }else{
		    	
		    	var url = ctxPath + "/chart/checkId.do";
		    	var param = "shopId=" + shopId +
		    				"&id=" + $("#id-input").val();
		    	
		    	$.ajax({
		    		url : url,
		    		data : param,
		    		type : "post",
		    		dataType : "text",
		    		success : function(data){
		    			var checkedId = data
		    			if(checkedId=="true"){
		    				passId=false;
		    				$("#id-alert").css({
								"color" : "red"
							})
		    				$("#id-alert").html("이미 존재하는 아이디 입니다.");
		    			}
		    			if(checkedId=="false"){
		    				passId=true;
		    				$("#id-alert").html("사용하실수 있는 아이디 입니다.");
		    				$("#id-alert").css({
		    					"color" : "green"
		    				})
		    			}
		    		}
		    	});
		    	
		    }
		}else{
			$("#id-alert").html("아이디는 5자 이상입니다.")
		}
	}
	
	function checkPw(e){
		if($("#pw-input").val().length>=8 && $("#pw-input-repeat").val().length>=8){
			if($("#pw-input").val()==$("#pw-input-repeat").val()){
				passPw=true;
				$("#pw-alert").css({
					"color" : "green"
				})
				$("#pw-alert").html("패스워드가 같습니다.");
			}else{
				passPw=false;
				$("#pw-alert").css({
					"color" : "red"
				})
				$("#pw-alert").html("서로 다른 패스워드입니다.");
			}
		}else{
			passPw=false;
			$("#pw-alert").css({
				"color" : "red"
			})
			$("#pw-alert").html("패스워드는 8자 이상입니다.");
		}
	}
	
	var passId=false;
	var passPw=false;
	
	function doSignIn(){
		if(passId && passPw){
			var url = ctxPath + "/chart/doSignIn.do";
	    	var param = "shopId=" + shopId +
	    				"&id=" + $("#id-input").val() +
	    				"&password=" + $("#pw-input").val();
			
			$.ajax({
	    		url : url,
	    		data : param,
	    		type : "post",
	    		dataType : "text",
	    		success : function(data){
	    			if(data){
	    				alert("회원 가입이 성공적으로 되었습니다.");
	    				dialog.close();
	    			}else{
	    				alert("회원 가입 실패");
	    			}
	    		}
	    	});
			
		}else{
			if(!passId){
				$("#id-input").focus();	
			}else if(!passPw){
				$("#pw-input").focus();	
			}
		}
	}

	

	
</script>

<script>
	$(function(){
		
	})
</script>
	

</head>
<body>	
	<div id="intro_back"></div>
	<span id="intro"></span>

	<div id="today"></div>
	<div id="title_right"></div>
	<a href=""><img alt="" src="${ctxPath }/images/aIdoo.png" id="aIdoo" ></a>
	<img alt="" src="${ctxPath }/images/grid.png" id="grid">
	
	<div id="container">
		<div id="panel">
			<table id="panel_table" width="100%">
			</table>
		</div>
		<Center>
			<table id="iconTable" style="width: 50%">
				<Tr>
					<td style="width: 50%">
						<img alt="" src="${ctxPath }/images/icons/Monitoring.png" id="monitor" name="Monitoring"><Br>		
					</td>
					<td style="width: 50%">
						<img alt="" src="${ctxPath }/images/icons/Analysis.png" id="analysis" name="Analysis"><Br>
					</td>
				</Tr>
				<tr>
					<%-- <td>
						<img alt="" src="${ctxPath }/images/icons/Tool Management.png" id="tool" name="Tool Management"><Br>
					</td> --%>
					<td>
						<img alt="" src="${ctxPath }/images/icons/Maintenance.png" id="maintenance" name="Maintenance"><Br>
					</td>
					
					<td>
						<img alt="" src="${ctxPath }/images/icons/Configuration.png" id="configuration" name="Configuration"><Br>
					</td>
				</tr>
			</table>	
		</Center>
		
		<!-- <span id="link">OLD</span> -->
	</div>
	<div id="dialog"></div>
	<div id="popup">계정 정보가 올바르지 않습니다.</div>
</body>
</html>	
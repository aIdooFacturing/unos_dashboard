package com.unomic.cnc;

public class Var {
	
	public static String IP = "192.168.0.171";
	public static String port = "9998";
	public static int soTimeout = 5000;
	public static int pingTimeout = 5000;
	public static int threadSleep = 1000;
	
	public static boolean dialog = false;
	
	public static int CMD_LOGIN_REQ = 0x01;
	public static int CMD_LOGIN_REP = 0x02;
	public static int CMD_LOGOUT_REQ = 0x03;
	public static int CMD_STATUSINFO_REQ = 0x10;
	public static int CMD_STATUSINFO_REP = 0x11;
	public static int CMD_FILETRANS_REQ = 0x21;
	public static int CMD_FILETRANS_REP = 0x22;
	
	public static final int SWIPE_MIN_DISTANCE = 50;
	public static final int SWIPE_MAX_OFF_PATH = 250;
	public static final int SWIPE_THRESHOLD_VELOCITY = 200;
	
	public static int byteLength = 8192;
	
	public static int idLength = 30;
	public static int pwLength = 20;
	public static int c2dmLength = 128;
	public static int fileNameLength = 32;
	
}

package com.unomic.cnc.data;

public class Axis 
{
	private String	axes_name;
	private String	axes;
	
	public Axis(String _axes_name, String _axes)
	{
		axes_name = _axes_name;
		axes = _axes;
	}
	
	public String getAxes_name() 
	{
		return axes_name;
	}

	public void setAxes_name(String axes_name) 
	{
		this.axes_name = axes_name;
	}

	public String getAxes() 
	{
		return axes;
	}

	public void setAxes(String axes) 
	{
		this.axes = axes;
	}

	public String toString()
	{
		return axes_name + " : " + axes.trim() + "\n";
	}
}

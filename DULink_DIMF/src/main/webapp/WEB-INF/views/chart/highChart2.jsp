<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"	content="width=device-width, user-scalable=no, initial-scale=1">
<title>Insert title here</title>
<script type="text/javascript" src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="${ctxPath }/js/svg/snap.svg.js"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/highcharts-3d.js"></script>
<script src="http://code.highcharts.com/highcharts-more.js"></script>
<script src="http://code.highcharts.com/modules/data.js"></script>
<script src="http://code.highcharts.com/modules/solid-gauge.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>

<link rel="stylesheet" href="http://github.hubspot.com/odometer/themes/odometer-theme-train-station.css" />
<script src="http://github.hubspot.com/odometer/odometer.js"></script>
<script type="text/javascript">
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth() + 1));
	var day = addZero(String(date.getDate()));
	var jeolboon;
	var today = year + "-" + month + "-" + day;

	window.odometerOptions = {
			  auto: false, // Don't automatically initialize everything with class 'odometer'
			  selector: '.my-numbers', // Change the selector used to automatically find things to be animated
			  format: 'd', // Change how digit groups are formatted, and how many digits are shown after the decimal point
			  duration: 10, // Change how long the javascript expects the CSS animation to take
			  theme: 'car', // Specify the theme (if you have more than one theme css file on the page)
			  animation: 'count' // Count is a simpler animation method which just increments the value,
			                     // use it when you're looking for something more subtle.
			};
	
	function time(){
		var date = new Date();
		var month = date.getMonth()+1;
		var date_ = addZero(String(date.getDate()));
		var day = date.getDay();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		if(day==1){
			day = "Mon";
		}else if(day==2){
			day = "Tue";
		}else if(day==3){
			day = "Wed";
		}else if(day==4){
			day = "Thu";
		}else if(day==5){
			day = "Fri";
		}else if(day==6){
			day = "Sat";
		}else if(day==0){
			day = "Sun";
		};
		
		$("#date").html(month + " / " + date_ + " (" + day + ") " + hour + " : " + minute + " : " + second);
	};
	
	$(function() {
		setInterval(time, 1000);
		getDvcData();
		setInterval(function(){
			operation = Math.floor(operationTime/totalTime*100);
		    $(".odometer").html(Math.round(operation));                
		});
		                     
		setElement();

		closeDoor();
		setInterval(banner, 5000);

		statusChart("statusChart1");
		statusChart("statusChart2");
		statusChart("statusChart3");

		statusBar = $('#statusChart1').highcharts();
		spindleChart("feed1", "Actual Feed", "feed");
		spindleChart("spindle1", "Spindle Actual Speed", "spindle");

		gaugeChart("gauge1");
		gaugeChart("gauge2");
		gaugeChart("gauge3");
		
		gaugeChart2("gauge1-2");
		gaugeChart2("gauge2-2");
		gaugeChart2("gauge3-2");
		
		circleChart("circleChart1");
		circleChart("circleChart2");
		circleChart("circleChart3");

		/* scatterChart("scatter1");
		scatterChart("scatter2");
		scatterChart("scatter3"); */
		 
		circleChart = $("#circleChart1").highcharts();			
		//setInterval(slideChart,10000);
	});

	var circleChart;
	function scatterChart(el){
		 $('#' + el).highcharts({
			 	chart: {
					backgroundColor : 'rgba(255, 255, 255, 0)',
            		type: 'scatter',
		        	zoomType: 'xy'
		        },
		      	credits : false,
		      	exporting : false,
		      	title: {
		      		text: false
		        },
		      	subtitle: {
		       	text: false
		        },
		      	xAxis: {
		      		title: {
		          	enabled: false,
		            },
	            	startOnTick: true,
	           	endOnTick: true,
	            	showLastLabel: true,
	            	labels : {
						style : {
							fontSize : '20px',
							fontWeight:"bold"
						}
					}
		        },
		      	yAxis: {
		     		min : 50,
		        	max : 100,
		        	title: {
		         		text: false
		            },
		         	labels : {
						style : {
							fontSize : '20px',
							fontWeight:"bold"
						}
					}
		        },
		      	legend: {
		     		enabled :false,
		        },
		      	plotOptions: {
		     		scatter: {
		         		marker: {
		            		radius: 5,
		              	states: {
		              		hover: {
		                 		enabled: true,
		                     	lineColor: 'rgb(100,100,100)'
		                        }
		                    }
		                },
		           	states: {
		            		hover: {
		               		marker: {
                         		enabled: false
		                        }
		                    }
		                },
		           	tooltip: {
                   		headerFormat: '<b>{series.name}</b><br>',
                   		pointFormat: '{point.x} cm, {point.y} kg'
		                }
		            }
		        },
		     	series: [{
           		name: 'Female',
	            	color: 'rgba(223, 83, 83, .5)',
	            	data: [[161.2, 51.6], [167.5, 59.0], [159.5, 49.2], [157.0, 63.0], [155.8, 53.6],
		                [170.0, 59.0], [159.1, 47.6], [166.0, 69.8], [176.2, 66.8], [160.2, 75.2],
		                [172.5, 55.2], [170.9, 54.2], [172.9, 62.5], [153.4, 42.0], [160.0, 50.0],
		                [147.2, 49.8], [168.2, 49.2], [175.0, 73.2], [157.0, 47.8], [167.6, 68.8],
		                [159.5, 50.6], [175.0, 82.5], [166.8, 57.2], [176.5, 87.8], [170.2, 72.8],
		                [174.0, 54.5], [173.0, 59.8], [179.9, 67.3], [170.5, 67.8], [160.0, 47.0],
		                [154.4, 46.2], [162.0, 55.0], [176.5, 83.0], [160.0, 54.4], [152.0, 45.8],
		                [162.1, 53.6], [170.0, 73.2], [160.2, 52.1], [161.3, 67.9], [166.4, 56.6],
		                [168.9, 62.3], [163.8, 58.5], [167.6, 54.5], [160.0, 50.2], [161.3, 60.3],
		                [167.6, 58.3], [165.1, 56.2], [160.0, 50.2], [170.0, 72.9], [157.5, 59.8],
		                [167.6, 61.0], [160.7, 69.1], [163.2, 55.9], [152.4, 46.5], [157.5, 54.3],
		                [168.3, 54.8], [180.3, 60.7], [165.5, 60.0], [165.0, 62.0], [164.5, 60.3],
		                [156.0, 52.7], [160.0, 74.3], [163.0, 62.0], [165.7, 73.1], [161.0, 80.0],
		                [162.0, 54.7], [166.0, 53.2], [174.0, 75.7], [172.7, 61.1], [167.6, 55.7],
		                [151.1, 48.7], [164.5, 52.3], [163.5, 50.0], [152.0, 59.3], [169.0, 62.5],
		                [164.0, 55.7], [161.2, 54.8], [155.0, 45.9], [170.0, 70.6], [176.2, 67.2],
		                [170.0, 69.4], [162.5, 58.2], [170.3, 64.8], [164.1, 71.6], [169.5, 52.8],
		                [163.2, 59.8], [154.5, 49.0], [159.8, 50.0], [173.2, 69.2], [170.0, 55.9],
		                [161.4, 63.4], [169.0, 58.2], [166.2, 58.6], [159.4, 45.7], [162.5, 52.2],
		                [159.0, 48.6], [162.8, 57.8], [159.0, 55.6], [179.8, 66.8], [162.9, 59.4],
		                [161.0, 53.6], [151.1, 73.2], [168.2, 53.4], [168.9, 69.0], [173.2, 58.4],
		                [171.8, 56.2], [178.0, 70.6], [164.3, 59.8], [163.0, 72.0], [168.5, 65.2],
		                [166.8, 56.6], [172.7, 105.2], [163.5, 51.8], [169.4, 63.4], [167.8, 59.0],
		                [159.5, 47.6], [167.6, 63.0], [161.2, 55.2], [160.0, 45.0], [163.2, 54.0],
		                [162.2, 50.2], [161.3, 60.2], [149.5, 44.8], [157.5, 58.8], [163.2, 56.4],
		                [172.7, 62.0], [155.0, 49.2], [156.5, 67.2], [164.0, 53.8], [160.9, 54.4],
		                [162.8, 58.0], [167.0, 59.8], [160.0, 54.8], [160.0, 43.2], [168.9, 60.5],
		                [158.2, 46.4], [156.0, 64.4], [160.0, 48.8], [167.1, 62.2], [158.0, 55.5],
		                [167.6, 57.8], [156.0, 54.6], [162.1, 59.2], [173.4, 52.7], [159.8, 53.2],
		                [170.5, 64.5], [159.2, 51.8], [157.5, 56.0], [161.3, 63.6], [162.6, 63.2],
		                [160.0, 59.5], [168.9, 56.8], [165.1, 64.1], [162.6, 50.0], [165.1, 72.3],
		                [166.4, 55.0], [160.0, 55.9], [152.4, 60.4], [170.2, 69.1], [162.6, 84.5],
		                [170.2, 55.9], [158.8, 55.5], [172.7, 69.5], [167.6, 76.4], [162.6, 61.4],
		                [167.6, 65.9], [156.2, 58.6], [175.2, 66.8], [172.1, 56.6], [162.6, 58.6],
		                [160.0, 55.9], [165.1, 59.1], [182.9, 81.8], [166.4, 70.7], [165.1, 56.8],
		                [177.8, 60.0], [165.1, 58.2], [175.3, 72.7], [154.9, 54.1], [158.8, 49.1],
		                [172.7, 75.9], [168.9, 55.0], [161.3, 57.3], [167.6, 55.0], [165.1, 65.5],
		                [175.3, 65.5], [157.5, 48.6], [163.8, 58.6], [167.6, 63.6], [165.1, 55.2],
		                [165.1, 62.7], [168.9, 56.6], [162.6, 53.9], [164.5, 63.2], [176.5, 73.6],
		                [168.9, 62.0], [175.3, 63.6], [159.4, 53.2], [160.0, 53.4], [170.2, 55.0],
		                [162.6, 70.5], [167.6, 54.5], [162.6, 54.5], [160.7, 55.9], [160.0, 59.0],
		                [157.5, 63.6], [162.6, 54.5], [152.4, 47.3], [170.2, 67.7], [165.1, 80.9],
		                [172.7, 70.5], [165.1, 60.9], [170.2, 63.6], [170.2, 54.5], [170.2, 59.1],
		                [161.3, 70.5], [167.6, 52.7], [167.6, 62.7], [165.1, 86.3], [162.6, 66.4],
		                [152.4, 67.3], [168.9, 63.0], [170.2, 73.6], [175.2, 62.3], [175.2, 57.7],
		                [160.0, 55.4], [165.1, 104.1], [174.0, 55.5], [170.2, 77.3], [160.0, 80.5],
		                [167.6, 64.5], [167.6, 72.3], [167.6, 61.4], [154.9, 58.2], [162.6, 81.8],
		                [175.3, 63.6], [171.4, 53.4], [157.5, 54.5], [165.1, 53.6], [160.0, 60.0],
		                [174.0, 73.6], [162.6, 61.4], [174.0, 55.5], [162.6, 63.6], [161.3, 60.9],
		                [156.2, 60.0], [149.9, 46.8], [169.5, 57.3], [160.0, 64.1], [175.3, 63.6],
		                [169.5, 67.3], [160.0, 75.5], [172.7, 68.2], [162.6, 61.4], [157.5, 76.8],
		                [176.5, 71.8], [164.4, 55.5], [160.7, 48.6], [174.0, 66.4], [163.8, 67.3]]

		        	}, 
		        	{
		            name: 'Male',
		            color: 'rgba(119, 152, 191, .5)',
		            data: [[174.0, 65.6], [175.3, 71.8], [193.5, 80.7], [186.5, 72.6], [187.2, 78.8],
		                [181.5, 74.8], [184.0, 86.4], [184.5, 78.4], [175.0, 62.0], [184.0, 81.6],
		                [180.0, 76.6], [177.8, 83.6], [192.0, 90.0], [176.0, 74.6], [174.0, 71.0],
		                [184.0, 79.6], [192.7, 93.8], [171.5, 70.0], [173.0, 72.4], [176.0, 85.9],
		                [176.0, 78.8], [180.5, 77.8], [172.7, 66.2], [176.0, 86.4], [173.5, 81.8],
		                [178.0, 89.6], [180.3, 82.8], [180.3, 76.4], [164.5, 63.2], [173.0, 60.9],
		                [183.5, 74.8], [175.5, 70.0], [188.0, 72.4], [189.2, 84.1], [172.8, 69.1],
		                [170.0, 59.5], [182.0, 67.2], [170.0, 61.3], [177.8, 68.6], [184.2, 80.1],
		                [186.7, 87.8], [171.4, 84.7], [172.7, 73.4], [175.3, 72.1], [180.3, 82.6],
		                [182.9, 88.7], [188.0, 84.1], [177.2, 94.1], [172.1, 74.9], [167.0, 59.1],
		                [169.5, 75.6], [174.0, 86.2], [172.7, 75.3], [182.2, 87.1], [164.1, 55.2],
		                [163.0, 57.0], [171.5, 61.4], [184.2, 76.8], [174.0, 86.8], [174.0, 72.2],
		                [177.0, 71.6], [186.0, 84.8], [167.0, 68.2], [171.8, 66.1], [182.0, 72.0],
		                [167.0, 64.6], [177.8, 74.8], [164.5, 70.0], [192.0, 101.6], [175.5, 63.2],
		                [171.2, 79.1], [181.6, 78.9], [167.4, 67.7], [181.1, 66.0], [177.0, 68.2],
		                [174.5, 63.9], [177.5, 72.0], [170.5, 56.8], [182.4, 74.5], [197.1, 90.9],
		                [180.1, 93.0], [175.5, 80.9], [180.6, 72.7], [184.4, 68.0], [175.5, 70.9],
		                [180.6, 72.5], [177.0, 72.5], [177.1, 83.4], [181.6, 75.5], [176.5, 73.0],
		                [175.0, 70.2], [174.0, 73.4], [165.1, 70.5], [177.0, 68.9], [192.0, 102.3],
		                [176.5, 68.4], [169.4, 65.9], [182.1, 75.7], [179.8, 84.5], [175.3, 87.7],
		                [184.9, 86.4], [177.3, 73.2], [167.4, 53.9], [178.1, 72.0], [168.9, 55.5],
		                [157.2, 58.4], [180.3, 83.2], [170.2, 72.7], [177.8, 64.1], [172.7, 72.3],
		                [165.1, 65.0], [186.7, 86.4], [165.1, 65.0], [174.0, 88.6], [175.3, 84.1],
		                [185.4, 66.8], [177.8, 75.5], [180.3, 93.2], [180.3, 82.7], [177.8, 58.0],
		                [177.8, 79.5], [177.8, 78.6], [177.8, 71.8], [177.8, 116.4], [163.8, 72.2],
		                [188.0, 83.6], [198.1, 85.5], [175.3, 90.9], [166.4, 85.9], [190.5, 89.1],
		                [166.4, 75.0], [177.8, 77.7], [179.7, 86.4], [172.7, 90.9], [190.5, 73.6],
		                [185.4, 76.4], [168.9, 69.1], [167.6, 84.5], [175.3, 64.5], [170.2, 69.1],
		                [190.5, 108.6], [177.8, 86.4], [190.5, 80.9], [177.8, 87.7], [184.2, 94.5],
		                [176.5, 80.2], [177.8, 72.0], [180.3, 71.4], [171.4, 72.7], [172.7, 84.1],
		                [172.7, 76.8], [177.8, 63.6], [177.8, 80.9], [182.9, 80.9], [170.2, 85.5],
		                [167.6, 68.6], [175.3, 67.7], [165.1, 66.4], [185.4, 102.3], [181.6, 70.5],
		                [172.7, 95.9], [190.5, 84.1], [179.1, 87.3], [175.3, 71.8], [170.2, 65.9],
		                [193.0, 95.9], [171.4, 91.4], [177.8, 81.8], [177.8, 96.8], [167.6, 69.1],
		                [167.6, 82.7], [180.3, 75.5], [182.9, 79.5], [176.5, 73.6], [186.7, 91.8],
		                [188.0, 84.1], [188.0, 85.9], [177.8, 81.8], [174.0, 82.5], [177.8, 80.5],
		                [171.4, 70.0], [185.4, 81.8], [185.4, 84.1], [188.0, 90.5], [188.0, 91.4],
		                [182.9, 89.1], [176.5, 85.0], [175.3, 69.1], [175.3, 73.6], [188.0, 80.5],
		                [188.0, 82.7], [175.3, 86.4], [170.5, 67.7], [179.1, 92.7], [177.8, 93.6],
		                [175.3, 70.9], [182.9, 75.0], [170.8, 93.2], [188.0, 93.2], [180.3, 77.7],
		                [177.8, 61.4], [185.4, 94.1], [168.9, 75.0], [185.4, 83.6], [180.3, 85.5],
		                [174.0, 73.9], [167.6, 66.8], [182.9, 87.3], [160.0, 72.3], [180.3, 88.6],
		                [167.6, 75.5], [186.7, 101.4], [175.3, 91.1], [175.3, 67.3], [175.9, 77.7],
		                [175.3, 81.8], [179.1, 75.5], [181.6, 84.5], [177.8, 76.6], [182.9, 85.0],
		                [177.8, 102.5], [184.2, 77.3], [179.1, 71.8], [176.5, 87.9], [188.0, 94.3],
		                [174.0, 70.9], [167.6, 64.5], [170.2, 77.3], [167.6, 72.3], [188.0, 87.3],
		                [174.0, 80.0], [176.5, 82.3], [180.3, 73.6], [167.6, 74.1], [188.0, 85.9],
		                [180.3, 73.2], [167.6, 76.3], [183.0, 65.9], [183.0, 90.9], [179.1, 89.1],
		                [170.2, 62.3], [177.8, 82.7], [179.1, 79.1], [190.5, 98.2], [177.8, 84.1],
		                [180.3, 83.2], [180.3, 83.2]]
		        }]
		    });	
	};
	
	function circleChart(el){
	    $('#' + el).highcharts({
	    	credits :false,
	    	exporting : false,
	     	chart: {
	     		backgroundColor : 'rgba(255, 255, 255, 0)',
            	polar: true,
            	type: 'column'
	        },
	      	title: {
          	text: false
	        },
        	subtitle: {
          	text: false
	        },
	      	pane: {
	      		size: '85%'
	        },
        	legend: {
				enabled :false
	        },
			xAxis: {
				tickmarkPlacement: 'on',
				 tickInterval: 1,
		            min: 0,
		            max: 24,
	        },
			yAxis: {
				min: 0,
				endOnTick: false,
				showLastLabel: true,
				title: {
					text: false
	            },
				labels: {
					enabled : false,
					formatter: function () {
					return this.value + '%';
	                }
	            },
				reversedStacks: false
	        },
			tooltip: {
				valueSuffix: '%'
	        },
			plotOptions: {
				series: {
					stacking: 'normal',
					shadow: false,
					groupPadding: 0,
					pointPlacement: 'on'
	            }
	        },
	        series: [{
	            type: 'column',
	            name: 'Column',
	            data: [9,8,9,8,7,8,9,6,9,7,8,7,9,8,6,5,7,8,9,8,7,8,8,8], 
	            color : "green"
	          
	        },
	          {
	            type: 'column',
	            name: 'Column',
	            data: [1,2,1,2,2,2,1,2,1,3,2,3,1,2,3,5,3,2,1,2,3,2,2,2],
	              color : "yellow"
	          
	        },      
	                 {
	            type: 'column',
	            name: 'Column',
	            data: [0,0,0,0,1,0,0,2,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0], 
	          color : "red"
	        },      
	                ]
	    });
	};
	
	function timer(){
		time++;
	};

	
	var spdGauge;
	function gaugeChart2(el){
		 var gaugeOptions = {
			chart: {
				type: 'solidgauge',
				backgroundColor : 'rgba(255, 255, 255, 0)'
			},
			title: null,
			exporting : false,
			credits : false,
			pane: {
				center: ['50%', '85%'],
				size: '100%',
				startAngle: -90,
				endAngle: 90,
				background: {
					backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
					innerRadius: '60%',
					outerRadius: '100%',
					shape: 'arc'
					}
				},
				tooltip: {
					enabled: false
				},
				yAxis: {
					stops: [
						[0.1, '#55BF3B'], // green
						[0.5, '#DDDF0D'], // yellow
						[0.9, '#DF5353'] // red
					],
					lineWidth: 0,
					minorTickInterval: null,
					tickPixelInterval: 400,
					tickWidth: 0,
						title: {
							y: -70
			            },
						labels: {
							y: 16
			            },
						style : {
			            	enabled : false
			            }
			        },
			      	plotOptions: {
						solidgauge: {
							dataLabels: {
								y: 5,
									borderWidth: 0,
			                   useHTML: true
			                }
			            }
			        }
			    };

			    // The speed gauge
			    $('#' + el).highcharts(Highcharts.merge(gaugeOptions, {
			        yAxis: {
			            min: 0,
			            max: 160,
			            title: {
			                text: false
			            	},
			            	labels : {
			            		enabled : false		
			            	}
			           	
			        },
			      	credits: {
			      		enabled: false
			        },
			      	series: [{
			     		data: [0],
			         	dataLabels: {
			        		format: '<div style="text-align:center"><span style="font-size:50px;color:' +
			                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
			                       '<span style="font-size:25px;color:silver">Spindle Load</span></div>'
			            },
			         	tooltip: {
			         		valueSuffix: ' km/h'
			            }
			        }]
			    }));

			    // Bring life to the dials
			    setInterval(function () {
			      getSpdGaugeData();
			    }, 1000);
	};
	
	function getSpdGaugeData(){
		spdGauge = $('#gauge1-2').highcharts();		
		var point = spdGauge.series[0].points[0];
		point.update(Number(_spdLoad));
	};
	
	function openDoor(){
		$("#svg").html("");
		
		$("#leftDoor").animate({
			"left" : -window.innerWidth/2
		}, 1000);
		
		$("#rightDoor").animate({
			"right" : -window.innerWidth/2
		}, 1000); 
		
		$("#logo_left").animate({
			"right" : window.innerWidth
		}, 1000);
		
		$("#logo_right").animate({
			"left" : window.innerWidth
		}, 1000);
		
		//setTimeout(closeDoor, 5000);
		
		pageIndex++;
		if(pageIndex==4)pageIndex=1;
	};
	
	var pageIndex = 1;
	function closeDoor(){
		$("#leftDoor").animate({
			left : 0
		},1000, switchPage);
		
		$("#rightDoor").animate({
			right : 0
		},1000);
		
		$("#logo_left").animate({
			"right" : window.innerWidth/2
		}, 1000);
		
		$("#logo_right").animate({
			"left" : window.innerWidth/2
		}, 1000);
	};
	
	function switchPage(){
		//showHourGlass();
		
		$(".container").css("z-index",1);
		$("#container" + pageIndex).css("z-index",2);
		setTimeout(openDoor, 1500);
	};
	
	function setVideoAnim(){
		$("#img1, #img2, #img3").animate({
			"width" : 1000,
			"height" : 1000,
			"left" : 400,
			"top" : 200
		});
		
		setInterval(function(){
			$("#img1, #img2, #img3").animate({
				"width" : 400,
				"height" : 400,
				"left" : 1500,
				"top" : 220
			});	
		}, 2000);
		
	};
	
	function slideChart() {
		$(".slide1").animate({
			"left" : -$(".container").width()
		}, 1500, function() {
			$(this).css("left", $(".container").width());
			$(this).removeClass("slide1");
			$(this).addClass("slide3");
		});

		$(".slide2").animate({
			"left" : 0
		}, 2000, function() {
			$(this).removeClass("slide2");
			$(this).addClass("slide1");
		});

		$(".slide3").animate({

		}, 2000, function() {
			$(this).removeClass("slide3");
			$(this).addClass("slide2");
		});
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function removeHypen(str){
		str = str.replace(/-/gi,"");
		return str;
	};
	
	var _spdLoad;
	var start_time;
	var operationTime = 0;
	var totalSecond = 0;
	var totalTime = 0;
	function getDvcData(){
		var url = "${ctxPath}/chart/getStatusData.do";
		var param = "dvcId=" + 41 + 
						"&date=" + today;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.status;
			
				$(json).each(function(i, data){
					var startTime = removeHypen(data.startDateTime).substr(9,8);
					var endTime = removeHypen(data.endDateTime).substr(9,8);
					var chartStatus = data.chartStatus;
					var alarm = data.alarm;
					var spdLoad = data.spdLoad;
					var spdActualSpeed = data.spdActualSpeed;
					var actualFeed = data.actualFeed;
					var feedOverride = data.feedOverride;
					var date = data.date;
					
					_spdLoad = spdLoad;
					spindle = spdActualSpeed;
					feed = actualFeed;
					
					//console.log(startTime, endTime, chartStatus, alarm, spdLoad, feedOverride, date)
					
					
					var startH = Number(startTime.substr(0,2) * 60);
					var startM = Number(startTime.substr(3,2));
					var startS = Number(startTime.substr(6,2));
					//startM = Number(String(startM).substr(0,String(startM).length-1) + "0");
					
				 	var endH = Number(endTime.substr(0,2) * 60);
					var endM = Number(endTime.substr(3,2)); 
					var endS = Number(endTime.substr(6,2));
					
					if(chartStatus=="In-Cycle"){
						operationTime += ((endH*60) + (endM*60) + endS) - ((startH*60) + (startM*60) + startS);
						if(Number(spdLoad)>0){
							spindleTime += ((endH*60) + (endM*60) + endS) - ((startH*60) + (startM*60) + startS);	
						};
					}else{
						
					};
					
					totalTime += ((endH*60) + (endM*60) + endS) - ((startH*60) + (startM*60) + startS);
					
					var stopTime = Number(startH + startM);
					var startPoint = Number(startTime.substr(0,2));
					var bar = 0;
					var color = "";
					
					if(chartStatus=="In-Cycle"){
						color = "green";	
					}else if(chartStatus=="Wait"){
						color = "yellow";
					}else if(chartStatus=="Alarm"){
						color = "red";
					};
					
					if(preMinute!=Number(startTime.substr(3,1))){
						if(Number(stopTime - preStopTime)>=20 && !first){
							statusBar.addSeries({
								data : [Math.floor((stopTime - preStopTime)/60) + String((stopTime - preStopTime)%60/10).substr(0,1)*block-block],
								color : "gray"
							});
							
							barIndex++;
						};
						
						//20시 초과
						if(startH>=(20 * 60) && firstOver20){
							console.log("chart reset")
							//chart reset
							for(var i = 0; i < barIndex; i++){
								statusBar.series[0].remove(true);
							}; 
							barIndex = 0;			
							/* statusBar.addSeries({
								data : [((startH+startM) - (20*60)) / 10 * block-block],
								color : "gray"
							}); */ 
							
							firstOver20 = false;
							currentOver20 = false;
						};
						
						drawRecordedData(startTime, endTime, chartStatus);
						preMinute=Number(startTime.substr(3,1));
						preStopTime = stopTime;
					};
					 
				});
				getCurrentDvcData();
			}, error : function(e1,e2,e3){
				console.log("network disconnected")
				var date = new Date();
				var hour = Number(addZero(String(date.getHours())));
				var minute = Number(addZero(String(date.getMinutes())).substr(0,1));
				
				var color = "";
				
				var bar = 0;
				
				if(hour>=20){
					bar = (hour-20) + (minute * block);
				}else if(hour<=19){
					bar = (hour+4) + (minute * block); 
				};
					
				if(firstError){
					for(var i = block, j = 0; i <= bar ; i+=block,j++){
						if(j==3 || j==23){
							color = "yellow";
						}else{
							color = "green";
							operationMinute += 10;
						};
						
						console.log(color)
						statusBar.addSeries({
							data : [block],
							color : color
						});	
						
						totalMinute += 10;
					};
					firstError = false;
				}else{
					if(preMinute!=minute){
						statusBar.addSeries({
							data : [block],
							color : "green"
						});	
					};
					preMinute=minute;
				};
				
				var random;
				random = Math.random();
				_spdLoad = Math.floor(60 + (random*10) + (random*10));
				
				random = Math.random();
				spindle = Math.floor(500 + (random*100) + (random*100));
				
				random = Math.random();
				feed = Math.floor(10000 + (random*1000) + (random*1000));
				
				jeolBoonValue = Math.floor(Math.random()*10 + 90);
				operation = 80;				
				setTimeout(getDvcData,1000);
			}
		});
	};
	
	function updateCircleChartData(status){
		var i;
		if(status=="In-Cycle"){
			i = 0;
		}else if(status=="Wait"){
			i = 1;
		}else if(status=="Alarm"){
			i = 2;
		};
		
		circleChart.seires[i].update({
			data : []		
		});
	};
	
	var firstError = true;
	var preStopTime = 0;
	var lastEndTime = 0;
	var spindleTime = 0;
	var lastSpindleTime = 0;
	function getCurrentDvcData(){
		var url = "${ctxPath}/chart/getCurrentDvcData.do";
		var param = "dvcId=" + 41 + 
						"&date=" + today;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var chartStatus = data.chartStatus;
				//parsingAlarm(data.alarm);
				spindle = data.spdActualSpeed;
				feed = data.actualFeed;
				_spdLoad = data.spdLoad;
				
				//if(Number(data.spdLoad)!=0)jeolBoonValue++;
				
				//jeolBoonValue = (operationMinute*60)
				var startTime = removeHypen(data.startDateTime).substr(9,8);
				var endTime = removeHypen(data.endDateTime).substr(9,8);
				
				var startH = Number(startTime.substr(0,2) * 60);
				var startM = Number(startTime.substr(3,2));
				var startS = Number(startTime.substr(6,2));
				
				var stopTime = Number(startH + startM);
				
			 	var endH = Number(endTime.substr(0,2) * 60);
				var endM = Number(endTime.substr(3,2));
				var endS = Number(endTime.substr(6,2));
				
			 	if(chartStatus=="In-Cycle" && lastSpindleTime != ((endH*60) + (endM*60) + endS) - ((startH*60) + (startM*60) + startS)){
			 		operationTime += ((endH*60) + (endM*60) + endS) - ((startH*60) + (startM*60) + startS);
			 		totalTime += ((endH*60) + (endM*60) + endS) - ((startH*60) + (startM*60) + startS);
					if(Number(data.spdLoad)>0){
						spindleTime += ((endH*60) + (endM*60) + endS) - ((startH*60) + (startM*60) + startS);	
					};
					lastSpindleTime = ((endH*60) + (endM*60) + endS) - ((startH*60) + (startM*60) + startS);
				}else if(chartStatus!="In-Cycle" && lastSpindleTime != ((endH*60) + (endM*60) + endS) - ((startH*60) + (startM*60) + startS)){
					totalTime += ((endH*60) + (endM*60) + endS) - ((startH*60) + (startM*60) + startS);
					lastSpindleTime = ((endH*60) + (endM*60) + endS) - ((startH*60) + (startM*60) + startS);
				};
				
				jeolBoonValue = Math.floor(spindleTime/operationTime*100);
				var color = "";
				
				if(chartStatus=="In-Cycle"){
					color = "green";	
				}else if(chartStatus=="Wait"){
					color = "yellow";
				}else if(chartStatus=="Alarm"){
					color = "red";
				};
				
				if(preMinute!=Number(startTime.substr(3,1))){
					if(Number(stopTime - preStopTime)>=10 && !first){
						
						statusBar.addSeries({
							data : [Math.floor((stopTime - preStopTime)/60) + String((stopTime - preStopTime)%60/10).substr(0,1)*block-block],
							color : "gray"
						});
						
						barIndex++;
					};
					
					//20시 초과
					if(startH>=(20 * 60) && currentOver20){
						console.log("chart reset")
						//chart reset
						for(var i = 0; i < barIndex; i++){
							statusBar.series[0].remove(true);
						}; 
						barIndex = 0;			
						/* statusBar.addSeries({
							data : [((startH+startM) - (20*60)) / 10 * block-block],
							color : "gray"
						});  */
						
						currentOver20 = false;
						
					};
					
					if(color=="green"){
						operationMinute += 10;
						operation = operationMinute/totalMinute*100;
					};
					statusBar.addSeries({
						data : [block],
						color : color
					});
					
					barIndex++;
				};
				
				
				//status 변화 시간이 10분 이하 && green -> wait || red
				if((startH==endH) && (Number(startTime.substr(3,1)) == Number(endTime.substr(3,1))) && ((preStatus=="In-Cycle" && chartStatus=="Wait") || (preStatus=="In-Cycle" && chartStatus=="Alarm") ||  (preStatus=="Wait" && chartStatus=="Alarm"))){
					console.log("color change : " + startTime + " - " + endTime);
					
					//chart y 값 = chart.series[index].data[0].y
					//var data = statusBar.series[barIndex].data[0].y - block;
					var pColor = "";
					if(preStatus=="In-Cycle"){
						pColor = "green";
					}else if(preStatus=="Wait"){
						pColor = "yellow";
					};
					
					//이전 상태 한 block 삭제
					statusBar.series[barIndex-1].remove(false);
					/* statusBar.addSeries({
						data : [data],
						color : pColor
					}); */
					
					//새로운 block draw
					statusBar.addSeries({
						data : [block],
						color : color
					});
				};
				
				var date = new Date();
				var hour = date.getHours()*60;
				var minute = date.getMinutes();
				var now = hour + minute;

				if((now - lastEndTime)>=10){
					console.log("power off");
					
					if(lastEndTime==0)lastEndTime = stopTime;
					statusBar.addSeries({
						data : [Math.floor((now - lastEndTime)/60) + String((now - lastEndTime)%60/10).substr(0,1)*block],
						color : "gray"
					}); 
					
					barIndex++;
					lastEndTime = now ;
					stopTime = now;
				};
				
				preMinute=Number(startTime.substr(3,1));
				setTimeout(getCurrentDvcData, 1000);
				preStatus = chartStatus;
				preStopTime = lastEndTime;
			}
		});
	};
	
	var spindle = 0;
	var feed = 0;
	function getActualData(series,ty){
		var x = (new Date()).getTime();
		var y = 0;
		if(ty=="spindle"){
			y = Number(spindle);
		}else if(ty=="feed"){
			y = Number(feed);	
		};
		series.addPoint([x,y], true, true);	
	};
	
	function spindleChart(id, title, ty) {
		 $(document).ready(function () {
		        Highcharts.setOptions({
		            global: {
		                useUTC: false
		            }
		        });

		        $('#' + id).highcharts({
		            chart: {
		            	backgroundColor : 'rgba(255, 255, 255, 0)',
		                type: 'spline',
		                animation: Highcharts.svg, // don't animate in old IE
		                marginRight: 10,
		                events: {
		                    load: function () {

		                        // set up the updating of the chart each second
		                        var series = this.series[0];
		                        setInterval(function () {
		                            	getActualData(series, ty)
		                        }, 1000);
		                    }
		                }
		            },
		         	title: {
		        		text: title,
		           	style : {
							fontSize : "40px"
		               	}
		                	
		            },
		            credits : false,
		            exporting : false,
		            xAxis: {
		                type: 'datetime',
		                tickPixelInterval: 150,
		                labels : {
							style : {
								fontSize : '20px',
								fontWeight:"bold"
							}
						}
		            },
		            yAxis: {
		            	/* min : 150, */
		            	/* max : 180, */
		            	/* tickInterval:10, */ 
		                title: {
		                    text: false
		                },
		                labels : {
							style : {
								fontSize : '20px',
								fontWeight:"bold"
							}
						},
		               /*  plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#ff0000'
		                }] */
		            },
		            tooltip: {
		                formatter: function () {
		                    return '<b>' + this.series.name + '</b><br/>' +
		                        Highcharts.dateFormat('%H:%M:%S', this.x) + '<br/>' +
		                        Highcharts.numberFormat(this.y, 2);
		                },
		                enabled : false
		            },
		            legend: {
		                enabled: false
		            },
		            exporting: {
		                enabled: false
		            },
		            plotOptions: {
		                spline: {
		                    lineWidth: 5,
		                    color : "#1093FF"
		                    },
		            },
		            series: [{
		                name: 'Random data',
		                data: (function () {
		                    // generate an array of random data
		                    var data = [],
		                        time = (new Date()).getTime(),
		                        i;

		                    for (i = -15; i <= 0; i += 1) {
		                        data.push({
		                            x: time + i * 1000,
		                            y: 0
		                        });
		                    }
		                    return data;
		                }()),
		            }]
		        });
		    });
	};

	function gaugeChart(id) {
		$('#' + id).highcharts({
			chart: {
				backgroundColor : 'rgba(255, 255, 255, 0)',
	         	type: 'gauge',
	         	plotBackgroundColor: null,
	         	plotBackgroundImage: null,
	        	plotBorderWidth: 0,
	         	plotShadow: false
	        },
			credits : false,
			exporting : false,
	      		title: {
	       		text: false
	        },
	        plotOptions: {
	            series: {
	                dataLabels: {
	                    enabled: true,
	                    style: {
	                        fontWeight: 'bold',
	                        fontSize : 40
	                   	 	},
	                   	 format: '{y}%'
	                }
	            }
	        },
	     	pane: {
	     		startAngle: -150,
	         	endAngle: 150,
	         	background: [{
	        		backgroundColor: {
	           		linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
	               		stops: [
	                  		[0, '#FFF'],
	                        	[1, '#333']
	                    	]
	                },
	            	borderWidth: 0,
	           	outerRadius: '109%'
	            }, 
	            {
	        	backgroundColor: {
	         		linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
	           		stops: [
	                  		[0, '#333'],
	                  	[1, '#FFF']
	                    ]
	          	},
	        	borderWidth: 1,
	        	outerRadius: '107%'
	            }, 
	            {}, 
	            {
	             backgroundColor: '#DDD',
	             borderWidth: 0,
	             outerRadius: '105%',
	            }]
	        },

	        // the value axis
	        yAxis: {
	            min: 0,
	            max: 100,

	            minorTickInterval: 'auto',
	            minorTickWidth: 1,
	            minorTickLength: 10,
	            minorTickPosition: 'inside',
	            minorTickColor: '#666',

	            tickPixelInterval: 30,
	            tickWidth: 2,
	            tickPosition: 'inside',
	            tickLength: 10,
	            tickColor: '#666',
	            labels: {
	                step: 2,
	                rotation: 'auto'
	            },
	            title: {
	                text: "절분율",
	                style:{
	                	fontSize : 40
	                }
	            },
	            plotBands: [{
	                from: 0,
	                to: 70,
	                color: '#55BF3B' // green
	            }, {
	                from: 70,
	                to: 90,
	                color: '#DDDF0D' // yellow
	            }, {
	                from: 90,
	                to: 100,
	                color: '#DF5353' // red
	            }]
	        },

	        series: [{
	            data: [80],
	            tooltip: {
	                valueSuffix: ' km/h'
	            }
	        }]

	    },
	        // Add some life
	        function (chart) {
	            if (!chart.renderer.forExport) {
	                setInterval(function () {
	               			getJeolBoonData();

	                }, 1000);
	            }
	        });
	};

	var jeolBoonValue=0;
	function getJeolBoonData(){
		var chart = $("#gauge1").highcharts();
		var point = chart.series[0].points[0];
		point.update(jeolBoonValue);	
	};
	
	var flag = true;
	function banner(){
		var duration = 2000;
		var center = $("#center");
		var doosan = $("#doosan");
		
		var $appear;
		var $disappear;
		
		if(flag){
			$appear = doosan;
			$disappear = center;
		}else{
			$appear = center;
			$disappear = doosan;
		};
		
		$disappear.animate({
			bottom : - $disappear.height()
		}, duration, function(){
			$disappear.css("bottom",window.innerHeight);
		});
		
		$appear.animate({
			bottom : 200
		}, duration);
		
		flag = !flag;
	};
	
	
	function setElement() {
		var width = window.innerWidth;
		var height = window.innerHeight;

		$(".container").css({
			"width" : width,
			"height" : height
		});

		$(".statusChart").css({
			"width" : 2900,
			"height" : 150
		});

		$(".gauge").css({
			"width" : width * 0.3,
			"height" : 500
		});
		
		
		$(".scatter, .feed").css({
			"width" : width * 0.3,
			"height" : 670
		});
		
		
		$(".gauge2").css({
			"width" : width * 0.3,
			"height" : 500
		});
		
		$(".circleChart").css({
			"width" : width * 0.3,
			"height" : 500
		});
		
		$(".spindle").css({
			"width" : 1000,
			"height" : 670
		});

		$(".daily").css({
			"width" : width * 0.3,
			"height" : 800
		});

		$("#center").css({
			"bottom" : 200
		});
		
		$("#doosan").css({
			"bottom" : window.innerHeight,
		});
		
		/* $("#container2").css({
			"left" : $(".container").width()
		});

		$("#container3").css({
			"left" : $(".container").width()
		}); */
		
		$("#leftDoor").css({
			position : "absolute",
			top : 0,
			left : 0,
			width : width/2,
			height : height,
			"z-index" : 99
		});
		
		$("#rightDoor").css({
			position : "absolute",
			top : 0,
			right : 0,
			width : width/2,
			height : height,
			"z-index" : 99
		});
		
		$("#logo_left").css({
			"height" : height * 0.25,
			"top" : height/2 - (height * 0.25/2),
			"position" : "absolute",
			"z-index" : 999,
			"right" : width/2
		});
		
		$("#logo_right").css({
			"top" : height/2 - (height * 0.25/2),
			"height" : height * 0.25,
			"position" : "absolute",
			"z-index" : 999,
			"left" : width/2
		});
		
		$("#svg").css({
			left : window.innerWidth/2 - $("#svg").width()/2,
			top : window.innerHeight/2 - $("#svg").height()/2
		});
		
	};

	var statusBar;
	function statusChart(id) {
		$('#' + id).highcharts({
			chart : {
				type : 'bar',
				backgroundColor : 'rgba(255, 255, 255, 0)'
			},
			credits : false,
			title : false,
			exporting : false,
			xAxis : {
				labels : {
					style : {
						fontSize : '25px'
					},
					enabled :false
				}
			},
			tooltip : {
				headerFormat : "",
				style : {
					fontSize : '20px',
				}
			},
			yAxis : {
				min : 0,
				max :24,
				showFirstLabel: true,
				showLastLabel: true,
				reversedStacks: false,
				endOnTick:false,
	         	tickInterval:1, 
				gridLineColor: 'black',
				title : {
					text : false
				},
				labels : {
					style : {
						fontSize : '25px',
					},
					enabled : false
				}
			},
			legend : {
				enabled : false
			},
			plotOptions : {
				series : {
					stacking : 'normal',
					pointWidth : 150
				}
			},
			series : []
		});
	};
	
	
	var block = 1/6;
	var bar;
	var first = true;
	var firstOver20 = true;
	var currentOver20 = true;
	var preMinute;
	var startPoint;
	var preStatus;
	var barIndex = 0;
	var totalMinute = 0;
	var operationMinute = 0;
	var operation;
	function drawRecordedData(startTime, endTime, status){
		var startH = Number(startTime.substr(0,2) * 60);
		var startM = Number(startTime.substr(3,2));
		var startS = Number(startTime.substr(6,2));
		//startM = Number(String(startM).substr(0,String(startM).length-1) + "0");
		
		var endH = Number(endTime.substr(0,2) * 60);
		var endM = Number(endTime.substr(3,2));
		var endS = Number(endTime.substr(6,2));
		//endM = Number(String(endM).substr(0,String(endM).length-1) + "0");
		
		var minute = Number(endH + endM) - Number(startH + startM);
		
		startPoint = Number(startTime.substr(0,2));
		
		//var cMinute = Number(endTime.substr(3,1));
		
		//preMinute = cMinute;
		
	/* 	console.log("경과 시간 : " + minute + " 분");
		console.log("상태 : " + status);
		console.log("블럭 크기 : " + Number(minute/10*block) + "\n\n"); */
		
		var color = "";
		
		if(first){
			start_time = startH+startM;
			if(startPoint>=20){
				bar = startPoint-20;
			}else if(startPoint<=19){
				bar = startPoint+4; 
			};
			
			color = "rgba(0,0,0,0)";
			statusBar.addSeries({
				data : [ bar + startTime.substr(3,1) * block ],
				color : color
			});
			
			first = !first;
			
			console.log("startingPoint : " + Number(bar + startTime.substr(3,1) * block));
			
			barIndex++;
		};
		
		if(status=="In-Cycle"){
			color = "green";	
			operationMinute += 10;
			operation = operationMinute/totalMinute*100;
		}else if(status=="Wait"){
			color = "yellow";
		}else if(status=="Alarm"){
			color = "red";
		};
		
		
		statusBar.addSeries({
			data : [block],
			color : color
		});
		
		totalMinute += 10;
		//20시 초과
		/* if(endH>=20 * 60 && firstOver20){
			//chart reset
			for(var i = 0; i <= barIndex; i++){
				statusBar.series[0].remove(true);
			};
			barIndex = -1;			
			statusBar.addSeries({
				data : [((endH+endM) - (20*60)) / 10 * block],
				color : color
			});
			firstOver20 = false;
			
		};
		
		//status 변화 시간이 10분 이하 && green -> wait || red
		if((startH==endH) && (Number(startTime.substr(3,1)) == Number(endTime.substr(3,1))) && ((preStatus=="In-Cycle" && status=="Wait") || (preStatus=="In-Cycle" && status=="Alarm") ||  (preStatus=="Wait" && status=="Alarm"))){
			console.log("color change : " + startTime + " - " + endTime);
			
			//chart y 값 = chart.series[index].data[0].y
			var data = statusBar.series[barIndex].data[0].y - block;
			var pColor = "";
			if(preStatus=="In-Cycle"){
				pColor = "green";
			}else if(preStatus=="Wait"){
				pColor = "yellow";
			};
			
			//이전 상태 한 block 삭제
			statusBar.series[barIndex].remove(true);
			statusBar.addSeries({
				data : [data],
				color : pColor
			});
			
			//새로운 block draw
			statusBar.addSeries({
				data : [block],
				color : color
			});
		};
		 */
		preStatus = status;
		barIndex++;
	};
	
	window.addEventListener("keydown", function (event) {
		if (event.keyCode == 39) {
			location.href="${ctxPath}/chart/highChart3.do";
		}else if (event.keyCode == 37) {
			location.href="${ctxPath}/chart/highChart1.do";
		};
	}, true);
</script>
<style type="text/css">
.odometer {
	z-index : 2;
    font-size: 150px;
    position: absolute;
}

#odometer1{
	top: 1350px;
	left: 540px;
}

body{
	overflow: hidden;
	 
}

*{
	margin: 0px;
	padding: 0px;
}
.img {
	position: absolute;
}

.spindle {
	position : absolute;
	margin-left: 1210px;
	top: 1395px
}

.circleChart{
	position: absolute;
	top:600px;
	right: 0px;
}

.daily {
	float: right;
	margin-right: 500px;
	margin-top: 200px;
}

.gauge {
	position: absolute;
	top: 600px;
	left: 1420px;
}

.gauge2{
	position: absolute;
	top: 600px;
	left: 2050px;
}


.scatter, .feed{
	position: absolute;
	top: 1395px;
	right: 300px;
}

.container {
	position: absolute;
	background: -webkit-linear-gradient(skyblue, white);
}

#bar {
	position: absolute;
	top: -40px;
	left: -40px;
	height: 2250px;
	z-index: 9;
}

#center, #doosan{
	position: absolute;
	z-index: 10;
	left: 10;
	width: 70px;
}

#img1, #img2, #img3{
	position: absolute;
	top: 545PX;
	left: 370px;
	width: 1280px;
	height: 680px;
}

#m1{
	top : 1255px;
	left: 50px;
}

#m2{
	top : 1350px;
	left: 170px;
}
#m3{
	top : 1350px;
	left: 170px;
}

.light{
	position :absolute;
	width: 50px;
}

#light1{
	top: 1800px;
	left: 1000px;
	z-index: 3;
}

#svg{
		position: absolute;
		z-index: 99999;
}

.statusChart{
	margin-top : 100px;
	z-index: 9;
}
.Boxshadow {
   /* -webkit-box-shadow: 3px 3px 5px 6px #ccc; */
   position: absolute;
   border-radius : 15px;
   /* background-color: #ff9f00; */
   /* border: 0.1em solid blue; */
}

#box1-0, #box2-0, #box3-0{
	width : 2900;
	height: 220px;
	top: 290px;
	left: 470px;
}
#box1-1, #box2-1, #box3-1{
	width: 1425px;
	height : 750px;
	top: 550px;
	left: 470px;
}

#box1-2, #box2-2, #box3-2{
	width: 1425px;
	height : 750px;
	top: 550px;
	right: 465px;
}

#box1-3, #box2-3, #box3-3{
	width: 1425px;
	height : 750px;
	top: 1350px;
	left: 470px;
}

#box1-4, #box2-4, #box3-4{
	width: 688;
	height : 750px;
	top: 1350px;
	left: 1945px;
}

#box1-5, #box2-5, #box3-5{
	width: 688px;
	height : 750px;
	top: 1350px;
	right: 465px;
}

.logo2{
	position : absolute;
	width: 300px;
	margin-top: 20px;
	margin-left: 200px;
	margin-bottom: 20px;
}

#logo1{
	position: absolute;
	width: 150px;
	margin-top: 20px;
	right: 100px;
}
#date{
	position: absolute;
	top: 200px;
	right: 100px;
	font-size: 30px;
	font-weight: bold;
}
</style>
</head>
<body>
	<!-- <svg id="svg" style="width:64px; height: 64px;" ></svg> -->
	<img alt="" src="${ctxPath }/images/DashBoard/logo_left.png" id="logo_left">
	<img alt="" src="${ctxPath }/images/DashBoard/logo_right.png" id="logo_right">
	<img src="${ctxPath }/images/DashBoard/left_door.jpg" id="leftDoor">
	<img src="${ctxPath }/images/DashBoard/right_door.jpg" id="rightDoor">
	
	<img src="${ctxPath }/images/machine/center.svg" id="center">
	<img src="${ctxPath }/images/machine/doosan.svg" id="doosan">
	<img src="${ctxPath }/images/machine/MetalBar.svg" id="bar" onclick="prePage();">
	
	<div id="container1" class="container slide1" >
		<font id="date"></font>
		<img src="${ctxPath }/images/DashBoard/logo2.png" id="logo1">
		<img src="${ctxPath }/images/DashBoard/doosan_logo.svg" class="logo2">
		<%-- <img src="${ctxPath }/images/DashBoard/logo2.png" class="logo2"> --%> 
		<div id="box1-0" class="Boxshadow"></div> 
		<br><br>
		<Center>
			<div id="title" style="font-size: 100px; font-weight: bold;" onclick="nextPage();">HF7M</div>
			<div id="statusChart1" class="statusChart"></div>
		</Center>
		
		<div style="width: 3000px; margin-left: 462px;">
					<font style="font-weight: bold; color:black; font-size: 28px">20</font>
					<font style="font-weight: bold; color:black; font-size: 28px; margin-left: 78px">21</font>
					<font style="font-weight: bold; color:black; font-size: 28px; margin-left: 80px">22</font>
					<font style="font-weight: bold; color:black; font-size: 28px; margin-left: 80px">23</font>
					<font style="font-weight: bold; color:black; font-size: 28px; margin-left: 80px">24</font>
					<font style="font-weight: bold; color:black; font-size: 28px; margin-left: 83px">01</font>
					<font style="font-weight: bold; color:black; font-size: 28px; margin-left: 83px">02</font>
					<font style="font-weight: bold; color:black; font-size: 28px; margin-left: 82px">03</font>
					<font style="font-weight: bold; color:black; font-size: 28px; margin-left: 81px">04</font>
					<font style="font-weight: bold; color:black; font-size: 28px; margin-left: 81px">05</font>
					<font style="font-weight: bold; color:black; font-size: 28px; margin-left: 80px">06</font>
					<font style="font-weight: bold; color:black; font-size: 28px; margin-left: 80px">07</font>
					<font style="font-weight: bold; color:black; font-size: 28px; margin-left: 80px">08</font>
					<font style="font-weight: bold; color:black; font-size: 28px; margin-left: 80px">09</font>
					<font style="font-weight: bold; color:black; font-size: 28px; margin-left: 81px">10</font>
					<font style="font-weight: bold; color:black; font-size: 28px; margin-left: 81px">11</font>
					<font style="font-weight: bold; color:black; font-size: 28px; margin-left: 80px">12</font>
					<font style="font-weight: bold; color:black; font-size: 28px; margin-left: 80px">13</font>
					<font style="font-weight: bold; color:black; font-size: 28px; margin-left: 81px">14</font>
					<font style="font-weight: bold; color:black; font-size: 28px; margin-left: 81px">15</font>
					<font style="font-weight: bold; color:black; font-size: 28px; margin-left: 80px">16</font>
					<font style="font-weight: bold; color:black; font-size: 28px; margin-left: 80px">17</font>
					<font style="font-weight: bold; color:black; font-size: 28px; margin-left: 80px">18</font>
					<font style="font-weight: bold; color:black; font-size: 28px; margin-left: 80px">19</font>
					<font style="font-weight: bold; color:black; font-size: 28px; margin-left: 80px">20</font>
				</div>
		
		<div id="box1-1" class="Boxshadow"></div>
		<div id="box1-2" class="Boxshadow"></div>
		<div id="box1-3" class="Boxshadow"></div>
		<div id="box1-4" class="Boxshadow"></div>
		<div id="box1-5" class="Boxshadow"></div>


		<div id="odometer1" class="odometer">0</div>

		<!-- <img src="http://192.168.0.54/mjpg/video.mjpg"   id="img1"> -->
		<%-- <img src="${ctxPath }/images/DashBoard/Green_Light.png" id="light1" class="light"> --%>
		<img src="${ctxPath }/images/machine/1.png" width="30%" class="img" id="m1">
		<div id="spindle1" class="spindle"></div>
		<div id="gauge1" class="gauge"></div>
		<div id="gauge1-2" class="gauge2"></div>
		
		<div id="circleChart1" class="circleChart"></div>
		
		<div id="feed1" class="feed"></div>
		
		
		
	</div>

	<div id="container2" class="container slide2">
		<img src="${ctxPath }/images/DashBoard/logo2.png" class="logo2"> 
		<!-- <div id="box1-0" class="Boxshadow"></div> -->
		<Center>
			<div id="title" style="font-size: 100px; font-weight: bold;" onclick="nextPage();">NB13W</div>
			<div id="statusChart2" class="statusChart"></div>
		</Center>

		<div id="box2-1" class="Boxshadow"></div>
		<div id="box2-2" class="Boxshadow"></div>
		<div id="box2-3" class="Boxshadow"></div>
		<div id="box2-4" class="Boxshadow"></div>
		
		<!-- <img src="http://192.168.0.54/mjpg/video.mjpg" id="img2"> -->
		<img src="${ctxPath }/images/machine/2.png" width="30%"  class="img" id="m2">
		<div id="spindle2" class="spindle"></div>
		<div id="daily2" class="daily"></div>
	</div>

	<div id="container3" class="container slide3">
		<img src="${ctxPath }/images/DashBoard/logo2.png" class="logo2"> 
		<Center>
		<!-- <div id="box1-0" class="Boxshadow"></div> -->
			<div id="title" style="font-size: 100px; font-weight: bold;" onclick="nextPage();">5FMS</div>
			<div id="statusChart3" class="statusChart"></div>
		</Center>

		<div id="box3-1" class="Boxshadow"></div>
		<div id="box3-2" class="Boxshadow"></div>
		<div id="box3-3" class="Boxshadow"></div>
		<div id="box3-4" class="Boxshadow"></div>
		
		<!-- <img src="http://192.168.0.54/mjpg/video.mjpg" id="img3"> -->
		<img src="${ctxPath }/images/machine/3.png" width="35%"  class="img" id="m3">
		
		<div id="spindle3" class="spindle"></div>
		<div id="daily3" class="daily"></div>
	</div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib_mobile.jsp"%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<title>Setting - Alarm</title>
<style>
body{
	background: url("../images/DashBoard/back_mobile.jpg");
	background-size : 105% 100%;
}

.title{
	top : 50px;
	position: absolute;
}

#title_main{
	z-index: 999;
}

#title_right{
	color : white;
}

#time{
	position: absolute;
	color: white;
} 
#date{
	position: absolute;
} 

hr{
	position: absolute;
	top: 250px;
	z-index: 99;
	width: 90%;
	border: 2px solid white;
}

#mainTable{
	width: 100%;
	z-index: 99;
	background: blue; /* Standard syntax */
 	opacity : 1;
 	text-align: center;
}

#header{
	width: 105%;
	z-index: -99;
	opacity : 1;
	background-color: blue;
}

#mainTable tr:last-child td:first-child {
    -moz-border-radius-bottomleft:10px;
    -webkit-border-bottom-left-radius:10px;
    border-bottom-left-radius:10px;
    
    -moz-border-radius-topleft:10px;
    -webkit-border-top-left-radius:10px;
    border-top-left-radius:10px
}

#mainTable tr:last-child td:last-child {
    -moz-border-radius-bottomright:10px;
    -webkit-border-bottom-right-radius:10px;
    border-bottom-right-radius:10px;
    
    -moz-border-radius-topright:10px;
    -webkit-border-top-right-radius:10px;
    border-top-right-radius:10px
}

#wraper{
	overflow: scroll;
	width : 105%;
}

.tr_table_fix_header
{
 position: relative;
 top: expression(this.offsetParent.scrollTop);
 z-index: 20;
}

.loading-overlay{
	z-index: 9999999999999 !important;
}

</style>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/jquery.loading.min.css">
<script type="text/javascript" src="${ctxPath }/js/jquery.loading.min.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>

<script type="text/javascript">
	
	var shopId = 1;
	
	var broswerInfo = navigator.userAgent;
	
	$(function(){
		
		getComName();
		setElement();
		
		setInterval(time, 1000);
		
		getBarChartDvcId2()
		
		setInterval(function(){
			getBarChartDvcId2()
		}, 5000)	
		
	});

	function isMobile() {
	    return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
	}
	
	function getTime(){
		var url = "${ctxPath}/chart/getTime.do";
		var time = "";
		$.ajax({
			url : url,
			dataType : "text",
			type : post,
			success : function(data){
				time = data;	
			}
		});
		
		return time;
	};
	
	function getMachineOrder(array, item) {
 		var rtn;
		for (var i = 0; i < array.length; i++) {
 			for (var j = 0; j < array[i].length; j++) {
 				if (array[i][j] == item) {
 					rtn = array[i][j+1]; 
 				};
 	        };
 	    };
		return rtn;
	};
	
	function getBarChartDvcId2(){
		var url = "${ctxPath}/chart/getAllDvcId.do";
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var today = year + "-" + month + "-" + day; 
		
		var param = "workDate=" + today + 
					"&shopId=" + shopId;
		
		
		
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			data : param,
			success : function(json){
				//console.log(json)
				var obj = json.dvcId;
				$('#mainTable tr:not(:first)').remove();
				$(obj).each(function(i, data){
					if(data.chartStatus=="CUT"){
						data.chartStatus="IN-CYCLE"
					}
					var operationTime = Number(data.operationTime/60/60).toFixed(1);
					var tr = "<tr onclick='goMobilePage(" + data.dvcId + ")'>" + 
									"<td style='font-size: "+getElSize(160)+"px; font-weight: bold; padding: "+getElSize(40)+"px;' valign='top' >" + decodeURIComponent(data.jig) + "</td>" +
									"<td style='font-size: "+getElSize(160)+"px; font-weight: bold; padding: "+getElSize(40)+"px;' valign='top' >" + decodeURIComponent(data.WC) + "</td>" +
									"<td style='font-size: "+getElSize(160)+"px; font-weight: bold; padding: "+getElSize(40)+"px;' valign='top' >" + data.name + "</td>" + 
									"<td style='font-size: "+getElSize(160)+"px; font-weight: bold; padding: "+getElSize(40)+"px;' valign='top' align='center'><img src=" + ctxPath + "/images/DashBoard/" + data.chartStatus +".png width='50px'></td>" + 
									"<td style='font-size: "+getElSize(160)+"px; font-weight: bold; padding: "+getElSize(40)+"px;' valign='top' >" + operationTime + "</td>" + 
								"</tr>";
								
					$("#mainTable").append(tr);
					
					$("#mainTable").css({
						"height" : 500
					});
				});
			},
			error : function(e1,e2,e3){
			}
		});
	};
	
	function goMobilePage(dvcId){
		window.sessionStorage.setItem("dvcId", dvcId);
		location.href="${ctxPath}/chart/mobile2.do";
	};
	
	function time(){
		var date = new Date();
		var month = date.getMonth()+1;
		var date_ = addZero(String(date.getDate()));
		var day = date.getDay();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		if(day==1){
			day = "Mon";
		}else if(day==2){
			day = "Tue";
		}else if(day==3){
			day = "Wed";
		}else if(day==4){
			day = "Thu";
		}else if(day==5){
			day = "Fri";
		}else if(day==6){
			day = "Sat";
		}else if(day==0){
			day = "Sun";
		};
		
		$("#date").html(month + " / " + date_ + " (" + day + ")");
		$("#time").html(hour + " : " + minute + " : " + second);
	};
	
	function setElement(){
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("#wraper").css({
			"height" : height * 0.73
		});
		
		$("#title_main").css({
			"left" : "30%",
			"width" : getElSize(1600),
			"margin-top" : getElSize(50)
		});
		
		$("#title_left").css({
			"left" : getElSize(100)
		});
		
		
		
		$("hr").css({
			"left" : width/2 - $("hr").width()/2
		});
		
		$(".status").css({
			"width" : width*0.45,
			"height" : height*0.8
		});
		
		$("#login-img").css({
			"position" : "absolute",
			"font-size" : getElSize(100),
			"top" : getElSize(50),
			"right" : getElSize(-120),
			"border-radius" : getElSize(15),
			"width" : getElSize(1200),
			"height" : getElSize(400)
		})
		
		
		$(".btn").css({
			"font-size" : getElSize(150),
			"background" : "white",
			"border" : getElSize(10)+"px solid black",
			"border-radius" : getElSize(20),
			"width" : getElSize(1500),
			"padding" : getElSize(100)
		})
		
		$("#setting-btn").css({
			"position" : "absolute",
			"font-size" : getElSize(200),
			"top" : getElSize(520),
			"right" : getElSize(-120),
			"border-radius" : getElSize(55),
			"width" : getElSize(1200),
			"height" : getElSize(400),
			"font-weight" : "bolder",
			"line-height" : 0,
			"z-index" : "1"
		})
		
		$("#setting-btn2").css({
			"position" : "absolute",
			"font-size" : getElSize(200),
			"top" : getElSize(990),
			"right" : getElSize(-120),
			"border-radius" : getElSize(55),
			"width" : getElSize(1200),
			"height" : getElSize(400),
			"font-weight" : "bolder",
			"line-height" : 0,
			"z-index" : "1"
		})
		
		$("#setting-btn3").css({
			"position" : "absolute",
			"font-size" : getElSize(200),
			"top" : getElSize(1460),
			"right" : getElSize(-120),
			"border-radius" : getElSize(55),
			"width" : getElSize(1200),
			"height" : getElSize(400),
			"font-weight" : "bolder",
			"line-height" : 0,
			"z-index" : "1"
		})
		
		$("#date").css({
			"left" : getElSize(100),
			"top" : getElSize(700),
			"color" : "white",
			"font-size" : getElSize(100),
			"font-weight" : "bolder",
		})
		
		$("#time").css({
			"left" : getElSize(900),
			"top" : getElSize(700),
			"color" : "white",
			"font-size" : getElSize(100),
			"font-weight" : "bolder"
		})
		
		$("#header").css({
			"height" : getElSize(900)
		})
		
		$("#title_right").css({
			"font-size" : getElSize(100),
			"right" : getElSize(50),
			"top" : getElSize(700)
		})
		
		$("#gear_img").css({
			"position" : "absolute",
			"right" : getElSize(-120),
			"width" : getElSize(400),
			"height" : getElSize(400),
			"top" : getElSize(50),
			"transition-duration": "0.5s",
			"z-index" : "1000",
			"border-radius" : getElSize(55)
		})
		
		$("#btn-img").css({
			"height" : getElSize(300),
			"width" : getElSize(300)
		})
		
		$("#name-div").css({
			"position" : "absolute",
			"top" : getElSize(700),
			"left" : getElSize(1800),
			"color" : "white",
			"font-size" : getElSize(100)
		})
		
		if(broswerInfo.indexOf("APP_AIDOO")>-1){
			
			$("#login-img").css({
				"display" : "none"
			})
			
			$("#setting-btn").css({
				"display" : "none"
			})
			
			$("#setting-btn2").css({
				"display" : "none"
			})
			
			$("#setting-btn3").css({
				"display" : "none"
			})
			
		}else{
			$("#login-img").css({
				"display" : "none"
			})
			
			$("#setting-btn").css({
				"display" : "none"
			})
			
			$("#setting-btn2").css({
				"display" : "none"
			})
			
			$("#setting-btn3").css({
				"display" : "none"
			})
			
			$("#gear_img").css({
				"display" : "none"
			})
		}
		
		
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function closeSetting(){
		location.href="${ctxPath}/chart/mobile2.do";
	};
	
</script>
</head>
<body>

	<script>
	
	function decode(str){
		return decodeURIComponent(str).replace(/\+/gi, " ")
	};
	
	var dialog;
	
	var userId;
	var settingDialog;
	var settingDialogDisplay;
	var settingDialogAccount;
	
	function pushSave(){
		
		
		$('body').loading({
			message: '기다려 주세요',
			theme: 'dark'
		});
		
		console.log(saveList)
		
		var url = ctxPath + "/chart/pushSave.do";
		var param = "id=" + userId + 
					"&shopId=" + shopId +
					"&list=" + saveList + 
					"&uuid=" + uuid;
		
		console.log(url+param)
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				
				$('body').loading('stop');
				
				if(data=="success"){
					alert("저장이 완료 되었습니다.")
					settingDialog.close();
				}
			}
		})
		
	}
	
	var saveList=[];
	var settingGrid;
	
	$(function(){
						
		var url = ctxPath + "/chart/getCheckedList.do";
		var param = "id=" + userId + 
					"&shopId=" + shopId + 
					"&uuid=" + uuid;
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				
				kendodata = new kendo.data.DataSource({
					schema: {
			           model: {
			               id: "dvcId",
			               fields: {
			            	   name : {},
			            	   jig : {},
			            	   checked : {},
			            	   dvcId : {}
			               }
			           }
					},
					group: { field: "jig" }
				})
				
				for(var i=0;i<json.length;i++){
					json[i].jig=decode(json[i].jig)
					kendodata.add(json[i])
				}
				
				$("#setting-grid").css({
					"width" : getElSize(3300)
				})
				
				$("#setting-grid").kendoGrid({
					dataSource : kendodata,
		            persistSelection: true,
					height : getElSize(4000),
					columns : [
						{ 
							selectable: true, 
							width: getElSize(100), 
							headerAttributes: {
							      style: "font-size : " + getElSize(150)
					    	},	
							attributes: {
							      style: "font-size : " + getElSize(150)
					    	}	
						},
						{ 
							field :"name",
							width: getElSize(300),
							headerTemplate : "장비명",
							headerAttributes: {
							      style: "font-size : " + getElSize(150)
					    	},
							attributes: {
						      style: "font-size : " + getElSize(150)
					    	}
						},
						{ 
							field :"jig",
							width: getElSize(200),
							headerTemplate : "직",
							groupHeaderTemplate: "#=value#",
							headerAttributes: {
							      style: "font-size : " + getElSize(150)
					    	},
							attributes: {
						      style: "font-size : " + getElSize(150)
					    	}
						},
					],
					change :function(arg){
						console.log("Checked")
						console.log(this.selectedKeyNames())
						
						saveList = this.selectedKeyNames()
					},
					dataBinding : function(){
						$("#setting-grid").css({
							"top" : "0"
						})
					},
					dataBound : function(){
						$(".k-no-text").css({
							"-webkit-transform": "scale(3)",
							"margin-bottom" : getElSize(-25),
							"margin-left" : getElSize(30)
						})
						
						$(".k-reset").css({
							"font-size" : getElSize(150)
						})
						
						$(".k-checkbox-label").first().css({
							"margin-bottom" : getElSize(70),
						})
						
						$(".btn").css({
							"font-size" : getElSize(150),
							"background" : "white",
							"border" : getElSize(10)+"px solid black",
							"border-radius" : getElSize(20),
							"width" : getElSize(2200),
							"padding" : getElSize(20),
							"margin-bottom" : getElSize(20),
							"margin-top" : getElSize(20)
						})
						
						$(".save-setting").css({
							"background" : "#4DA63A",
							"color" : "white",
							"margin-left" : getElSize(50),
							"padding" : getElSize(100),
							"width" : getElSize(3000),
							"margin-top" : getElSize(200)
						})
						
						$(".close-setting").css({
							"background" : "#F1F1F1",
							"color" : "black",
							"padding" : getElSize(100),
							"margin-left" : getElSize(50),
							"width" : getElSize(3000),
							"margin-top" : getElSize(200)
						})
						
						
						var grid = this;
						
						grid.items().each(function(){
							
							var data = grid.dataItem(this);
							
							if(data.checked==1){
								grid.select(this)
							}						
						})
					}
				})
			}
		});
	})
	
	function setuuid(args){
		uuid = args;
	}
		
	var userId = {};
	var uuid;
	
	</script>



	<Center>
		<div id="header" ></div>
	</Center>	
	<img id="login-img" onclick="logout()" src="/aIdoo/images/logout.png"></img>
	
	<button id="setting-btn" onclick="openSetting()">알림 설정</button>
	<button id="setting-btn2" onclick="openSettingDisplay()">화면 설정</button>
	<button id="setting-btn3" onclick="openSettingAccount()">계정 정보</button>
	
	<img src="${ctxPath }/images/DashBoard/title_left.svg" id="title_left" class="title">
	<img src="${ctxPath }/images/DashBoard/title_main.svg" id="title_main" class="title">
	<button id="gear_img" class="title" onclick="openSettingBtn()"><img src="${ctxPath }/images/Gear.png" id="btn-img"></button>
	
	<div id="title_right" class="title"></div>
	
	<div id="name-div"></div>
	
	<font id="date"></font>
	<font id="time"></font>
	
	<br>
	<br>
	<center>
		<div id="wraper">
			<table id="mainTable" border="2px" style="color: white" > 
				<tr style="font-size: 40px; font-weight: bold;" class="tr_table_fix_header">
					<td align="center">
						직
					</td>
					<td align="center">
						WC구분	
					</td>
					<td align="center">
						장비
					</td>
					<td align="center">
						가동상태
					</td>
					<td align="center">
						가동시간
					</td>
				</tr>
			</table>
		</div>
	</center>
	
	<div id="login-dialog">
		<table id="login-table">
			<tr id="img-tr">
				<td>
					<center><img src="/aIdoo/images/SmartFactory.png" id="logo" style="width: 219.213px; margin-top: 21.9213px;"></center>
					<center><img src="/aIdoo/images/logo.png" id="logo2" style="width: 131.528px; margin-bottom: 21.9213px;"></center>
				</td>
			</tr>
			<tr>
				<td>
					<center>
						<input id="id-input" autocomplete="off" onkeyup="this.value=this.value.replace(/[^a-zA-Z0-9]/g,'');"  type="text" maxlength=20 placeholder="ID">
						<br>
						<input id="password-input" autocomplete="off" onkeyup="this.value=this.value.replace(/[^a-zA-Z0-9]/g,'');"  type="password" maxlength=20 placeholder="PASSWORD">
					</center>
				</td>
			</tr>
			<tr>
				<td>
					<center>
					<button class="btn" id="btn-login" onclick="login()">LOGIN</button>
					<br>
					<button class="btn"  id="btn-sign" onclick="signUp()">가입하기</button>
					</center>
				</td>
			</tr>
		</table>
	</div>
	
	<div id="signUp-dialog">
		<table id="sign-login-table">
			<tr id="sign-img-tr">
				<td>
					<center><img src="/aIdoo/images/SmartFactory.png" id="sign-logo"></center>
					<center><img src="/aIdoo/images/logo.png" id="sign-logo2"></center>
				</td>
			</tr>
			<tr>
				<td>
					<center>
						<input class="id" id="sign-id-input" onkeyup="this.value=this.value.replace(/[^a-zA-Z0-9]/g,'');" autocomplete="off" type="text" maxlength=20 placeholder="ID">
						<br>
						<input class="name" id="sign-name-input" autocomplete="off" type="text" maxlength=10 placeholder="NAME">
						<br>
						<input id="sign-password-input" onkeyup="this.value=this.value.replace(/[^a-zA-Z0-9]/g,'');" autocomplete="off" type="password" maxlength=20 placeholder="PASSWORD">
						<br>
						<input id="sign-repeat-password-input" onkeyup="this.value=this.value.replace(/[^a-zA-Z0-9]/g,'');" autocomplete="off" type="password" maxlength=20 placeholder="PASSWORD">
					</center>
				</td>
			</tr>
			<tr>
				<td>
					<center>
					<button class="btn"  id="sign-btn-sign" onclick="setSignInfo()">가입하기</button>
					<br>
					<button class="btn"  id="close-btn" onclick="signDialogClose()">CLOSE</button>
					</center>
				</td>
			</tr>
		</table>
	</div>
	
	<div id="setting-dialog">
		<div id="setting-grid">
		</div>
		<button class="btn save-setting" onclick="pushSave()"> 저장 </button>
		<br>
		<button class="btn close-setting" onclick="settingDialogClose()"> 닫기 </button>
	</div>
	
	<div id="setting-dialog-display">
		<div id="setting-grid-display">
		</div>
		<button class="btn save-setting" onclick="pushSave()"> 저장 </button>
		<br>
		<button class="btn close-setting" onclick="settingDisplayDialogClose()"> 닫기 </button>
	</div>
	
	<div id="setting-dialog-account">
		<div id="setting-grid-account">
		</div>
		<button class="btn save-setting" onclick="pushSave()"> 저장 </button>
		<br>
		<button class="btn close-setting" onclick="settingAccountDialogClose()"> 닫기 </button>
	</div>
</body>
</html>